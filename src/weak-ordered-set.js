const symbols = {
    set: Symbol('set'),
    map: Symbol('map'),
    head: Symbol('head'),
    tail: Symbol('tail'),
    toNodeList: Symbol('toNodeList'),
    removeNode: Symbol('removeNode'),
    nextNode: Symbol('nextNode'),
    node: Symbol('node')
};

/**
 * An ordered set of weak references to objects. It can server as "ArrayLike" structure (see differences from array below), without preventing members of
 * this set to be garbage collected.
 *
 * This structure is both a little bit different from array and a set. Namely:
 * - Unlike array, this structure cannot use indices.
 * - Unlike array, this structure cannot store primitives, only objects (functions, arrays, Date, RegExp are objects).
 * - Unlike set, the items in this structure are ordered and the order can be modified.
 * - Unlike WeakMap, this structure can be iterated.
 *
 * This structure provides several ways to access its elements:
 * - If you have a (reference to) specific object, you can access it within the set, namely:
 * - - Check if it is in the set;
 * - - Retrieve the previous and the next object (if any);
 * - - Insert value before or after that object;
 * - Globally:
 * - - Insert values at the beginning or end of the collection;
 * - - Iterate through all values (possibly in reverse order);
 *
 * In JavaScript each object can be in one of three states: alive, dead, collected.
 * - Alive objects are those that are reachable be super-alive object. Super-alive objects those that are strongly referenced to the currently executing
 * stack, the microtask queue, to scheduled timer, the current context global object or the current isolate.
 * - Dead objects are objects still in memory, but not alive.
 * - Collected objects are those objects that no longer exists. The only reference to "collected" object inside JS environment is WeakRef.
 *
 * Garbage collerctor (GC) has the ability to convert dead objects to collected objects. It run when:
 * - Potentially (implementation-dependent) when the javascript is not currently executing a task;
 * - If attempting to execute a statement fails to allocate required memory;
 *
 * The way GC executes is implementation-dependent and can change in the future. However, it is almost certain that JavaScript will never throw
 * out-of-memory exception, while dead objects exists.
 *
 * As a result, the actual length of the {@link WeakOrderedSet} and indices within that structure can change even no user code access this structure.
 * Therefore, providing index-access and length getter is meaningless, their values would be wrong. If you want a structure that keep indices and length
 * create an Array of WeakRef instead. However, such structure does not guarantee that each item returned from an iteration is a reference to an existing
 * object.
 *
 * Due to how GC works, there is also no guarantee that if an object goes out-of-scope is collected on the next GC execution. If this set is iterated in
 * the same microtask as any statements that remove any strong reference to objects contained in this set, it is very likely those object still be returned.
 * The returned objects will be added to a string reference list until the current microtask finishes. This is an expected the standard of WeakRef and
 * there is nothing we can do about it.
 *
 */
export default class WeakOrderedSet {
    constructor() {
        this[symbols.map] = new WeakMap();
        this[symbols.head] = null;
        this[symbols.tail] = null;
    }

    unshift(...items) {
        const nodes = this[symbols.toNodeList](...items);
        if (nodes.length > 0) {
            if (this[symbols.head] == null) {
                if (this[symbols.tail] != null) {
                    const error = new Error('Invalid linked list state: tail != null while head == null');
                    error.code = 'ERR_LINKED_LIST_STATE';
                    throw error;
                }
                this[symbols.head] = nodes[0];
                this[symbols.tail] = nodes[nodes.length - 1];
            } else {
                nodes[nodes.length - 1].next = this[symbols.head];
                this[symbols.head].prev = nodes[nodes.length - 1];
            }
            for (const node of nodes) {
                const item = node.ref.deref();
                this[symbols.map].set(item, node);
            }
        }
        return this;
    }

    push(...items) {
        const nodes = this[symbols.toNodeList](...items);
        if (nodes.length > 0) {
            if (this[symbols.tail] === null) {
                if (this[symbols.head] != null) {
                    const error = new Error('Invalid linked list state: head != null while tail == null');
                    error.code = 'ERR_LINKED_LIST_STATE';
                    throw error;
                }
                this[symbols.head] = nodes[0];
                this[symbols.tail] = nodes[nodes.length - 1];
            } else {
                nodes[0].prev = this[symbols.tail];
                this[symbols.tail].next = nodes[0];
            }
            for (const node of nodes) {
                const item = node.ref.deref();
                this[symbols.map].set(item, node);
            }
        }
        return this;
    }

    shift() {
        while (this[symbols.head] != null) {
            const node = this[symbols.head];
            const item = node.ref.deref();
            const nextNode = node.next;
            if (nextNode != null) {
                nextNode.prev = null;
            } else {
                this[symbols.tail] = null;
            }
            this[symbols.head] = nextNode;
            if (item != null) {
                return item;
            }
        }
    }

    pop() {
        while (this[symbols.tail] != null) {
            const node = this[symbols.tail];
            const item = node.ref.deref();
            const prevNode = node.prev;
            if (prevNode != null) {
                prevNode.next = null;
            } else {
                this[symbols.head] = null;
            }
            this[symbols.tail] = prevNode;
            if (item != null) {
                return item;
            }
        }
    }

    has(value) {
        return this[symbols.map].has(value);
    }

    previous(value) {
        if (this[symbols.map].has(value)) {
            const node = this[symbols.map].get(value);
            let prevNode = node.prev;
            while (prevNode != null) {
                const item = prevNode.ref.deref();
                if (item != null) {
                    return item;
                }
                const prevNode2 = prevNode.prev;
                if (prevNode2 != null) {
                    prevNode2.next = prevNode.next;
                } else {
                    if (this[symbols.head] !== prevNode) {
                        const error = new Error('Invalid linked list state: head does not match a terminal node');
                        error.code = 'ERR_LINKED_LIST_STATE';
                        throw error;
                    }
                    this[symbols.head] = prevNode.next;
                    if (prevNode.next == null) {
                        this[symbols.tail] = null;
                    }
                }
                if (prevNode.next != null) {
                    prevNode.next.prev = prevNode2;
                }
                prevNode = prevNode2;
            }
        }
    }

    next(value) {
        if (this[symbols.map].has(value)) {
            const node = this[symbols.map].get(value);
            let nextNode = node.next;
            while (nextNode != null) {
                const item = nextNode.ref.deref();
                if (item != null) {
                    return item;
                }
                const nextNode2 = nextNode.next;
                if (nextNode2 != null) {
                    nextNode2.next = nextNode.prev;
                } else {
                    if (this[symbols.tail] !== nextNode) {
                        const error = new Error('Invalid linked list state: tail does not match a terminal node');
                        error.code = 'ERR_LINKED_LIST_STATE';
                        throw error;
                    }
                    this[symbols.tail] = nextNode.prev;
                    if (nextNode.prev == null) {
                        this[symbols.head] = null;
                    }
                }
                if (nextNode.prev != null) {
                    nextNode.prev.next = nextNode2;
                }
                nextNode = nextNode2;
            }
        }
    }

    insertBefore(item, reference, move = false) {
        if (!this[symbols.map].has(reference)) {
            const error = new ReferenceError(`This set does not contain the reference object`);
            error.code = 'ERR_NOT_FOUND';
            error.reference = reference;
            error.weakOrderedSet = this;
            throw error;
        }
        const refNode = this[symbols.map].get(reference);
        let itemNode;
        if (this[symbols.map].has(item)) {
            if (!move) {
                return false;
            }
            itemNode = this[symbols.map].get(item);
        } else {
            itemNode = Object.create(null);
            itemNode.ref = new WeakRef(item);
            itemNode.prev = itemNode.next = null;
        }
        if (refNode === itemNode) {
            const error = new ReferenceError(`The requested object cannot be inserted before itself`);
            error.code = 'ERR_LINK_REQUEST_ERROR';
            error.item = item;
            error.weakOrderedSet = this;
            throw error;
        }
        {
            const prevNode = itemNode.prev;
            const nextNode = itemNode.next;
            // Remove the itemNode if it is within the linked list
            // Note: The itemNode cannot be the only node, since refNode is there.
            if (prevNode != null) {
                if (prevNode === itemNode) {
                    return true;
                }
                prevNode.next = nextNode;
                if (nextNode == null) {
                    this[symbols.tail] = prevNode;
                }
            }
            if (nextNode != null) {
                nextNode.prev = prevNode;
                if (prevNode == null) {
                    this[symbols.head] = nextNode;
                }
            }
        }
        {
            const prevNode = refNode.prev;
            if (prevNode != null) {
                prevNode.next = itemNode;
            } else {
                if (this[symbols.head] !== refNode) {
                    const error = new Error('Invalid linked list state: head does not match a terminal node');
                    error.code = 'ERR_LINKED_LIST_STATE';
                    throw error;
                }
                this[symbols.head] = itemNode;
            }
            itemNode.next = refNode;
            itemNode.prev = prevNode;
            refNode.prev = itemNode;
        }
        return true;
    }

    insertAfter(item, reference, move = false) {
        if (!this[symbols.map].has(reference)) {
            const error = new ReferenceError(`This set does not contain the reference object`);
            error.code = 'ERR_NOT_FOUND';
            error.reference = reference;
            error.weakOrderedSet = this;
            throw error;
        }
        const refNode = this[symbols.map].get(reference);
        let itemNode;
        if (this[symbols.map].has(item)) {
            if (!move) {
                return false;
            }
            itemNode = this[symbols.map].get(item);
        } else {
            itemNode = Object.create(null);
            itemNode.ref = new WeakRef(item);
            itemNode.prev = itemNode.next = null;
        }
        if (refNode === itemNode) {
            const error = new ReferenceError(`The requested object cannot be inserted after itself`);
            error.code = 'ERR_LINK_REQUEST_ERROR';
            error.item = item;
            error.weakOrderedSet = this;
            throw error;
        }
        {
            const prevNode = itemNode.prev;
            const nextNode = itemNode.next;
            // Remove the itemNode if it is within the linked list
            // Note: The itemNode cannot be the only node, since refNode is there.
            if (nextNode != null) {
                if (nextNode === itemNode) {
                    return true;
                }
                nextNode.prev = prevNode;
                if (prevNode == null) {
                    this[symbols.head] = nextNode;
                }
            }
            if (prevNode != null) {
                prevNode.next = nextNode;
                if (nextNode == null) {
                    this[symbols.tail] = prevNode;
                }
            }
        }
        {
            const nextNode = refNode.next;
            if (nextNode != null) {
                nextNode.prev = itemNode;
            } else {
                if (this[symbols.tail] !== refNode) {
                    const error = new Error('Invalid linked list state: tail does not match a terminal node');
                    error.code = 'ERR_LINKED_LIST_STATE';
                    throw error;
                }
                this[symbols.tail] = itemNode;
            }
            itemNode.prev = refNode;
            itemNode.next = nextNode;
            refNode.next = itemNode;
        }
        return true;
    }

    delete(item) {
        if (!this[symbols.map].has(item)) {
            return this;
        }
        const node = this[symbols.map].get(item);
        this[symbols.removeNode](node);
        return this;
    }

    forwardIterator(reference = null) {
        let node;
        if (reference != null) {
            if (!this[symbols.map].has(reference)) {
                const error = new ReferenceError(`This set does not contain the reference object`);
                error.code = 'ERR_NOT_FOUND';
                error.reference = reference;
                error.weakOrderedSet = this;
                throw error;
            }
            node = this[symbols.map].get(reference);
        } else {
            node = this[symbols.head];
        }
        return new WeakOrderedSetForwardIterator(this, node);
    }

    backwardIterator(reference = null) {
        let node;
        if (reference != null) {
            if (!this[symbols.map].has(reference)) {
                const error = new ReferenceError(`This set does not contain the reference object`);
                error.code = 'ERR_NOT_FOUND';
                error.reference = reference;
                error.weakOrderedSet = this;
                throw error;
            }
            node = this[symbols.map].get(reference);
        } else {
            node = this[symbols.tail];
        }
        return new WeakOrderedSetBackwardIterator(this, node);
    }

    [Symbol.iterator]() {
        return this.forwardIterator();
    }

    [symbols.removeNode](node) {
        const prevNode = node.prev;
        const nextNode = node.next;
        // Removal of nodes should be done with care:
        // Removed node should not be modified (prev/next should be preserved), so any iterator holding reference to that node can continue...
        // However, consider scenario where:
        // List: ...-<0:node>-<1:node>-<2:node>-...
        // are removed from the weak ordered set's linked list.
        // If the node we are removing is <1:node>, it is not a problem: ...-<0:node>-<2:node>-...
        // because iterators holding <0:node> will now go directly to <2:node>, while iterators holding <1:node> would not be affected (<1:node> is not modified)
        // However, if the removed node is <0:node> or <2:node> its sibling is alive node in the set's linked list, that has already been modified.
        // In such case, the alive sibling should not be modified.
        // If we removing a terminal node, we should only update set's head/tail if the removed node is still alive.
        if (prevNode != null && prevNode.next === node) {
            prevNode.next = nextNode;
            if (nextNode == null && this[symbols.tail] === node) {
                this[symbols.tail] = prevNode;
            }
        }
        if (nextNode != null && nextNode.prev === node) {
            nextNode.prev = prevNode;
            if (prevNode == null && this[symbols.head] === node) {
                this[symbols.head] = nextNode;
            }
        }
        // At this point any set's head/tail containing the removed node should have been updated. This won't be true, if the removed node is the only
        // node in the set. In that case, we make the set empty.
        if (this[symbols.head] === node) {
            this[symbols.head] = null;
        }
        if (this[symbols.tail] === node) {
            this[symbols.tail] = null;
        }
    }

    [symbols.toNodeList](...items) {
        const nodes = [];
        for (const item of items) {
            if (!this[symbols.map].has(item)) {
                const node = Object.create(null);
                node.ref = new WeakRef(item);
                if (nodes.length > 0) {
                    node.prev = nodes[nodes.length - 1];
                    node.next = null;
                    nodes[nodes.length - 1].next = node;
                } else {
                    node.prev = node.next = null;
                }
                nodes.push(node);
            }
        }
        return nodes;
    }

    static isIterator(value) {
        return value instanceof WeakOrderedSetIterator;
    }

    static isForwardIterator(value) {
        return value instanceof WeakOrderedSetForwardIterator;
    }

    static isBackwardIterator(value) {
        return value instanceof WeakOrderedSetBackwardIterator;
    }
}

class WeakOrderedSetIterator {
    constructor(set, node) {
        this[symbols.set] = set;
        this[symbols.node] = node;
    }

    next() {
        const result = {
            done: false,
            value: void 0
        };
        while (this[symbols.node] != null) {
            const node = this[symbols.node];
            const object = node.ref.deref();
            if (object != null) {
                // If object is still alive return it...
                result.value = object;
                this[symbols.nextNode](node);
                return result;
            }
            // Otherwise, remove dead-ref node
            this[symbols.removeNode](node);
            // and continue with the next node
            this[symbols.nextNode](node);
        }
        // If no live reference is found and no more node to search, we are done
        result.done = true;
        return result;
    }

    [symbols.removeNode](node) {
        this[symbols.set][symbols.removeNode](node);
    }

    [symbols.nextNode]() {
        throw new TypeError('Illegal invocation: abstract method');
    }

    [Symbol.iterator]() {
        return this;
    }
}

class WeakOrderedSetForwardIterator extends WeakOrderedSetIterator {
    [symbols.nextNode]() {
        this[symbols.node] = this[symbols.node].next;
    }
}

class WeakOrderedSetBackwardIterator extends WeakOrderedSetIterator {
    [symbols.nextNode]() {
        this[symbols.node] = this[symbols.node].prev;
    }
}
