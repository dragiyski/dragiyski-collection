import regexpIdentifier from './regexp.identifier.js';

const JavaScript = {
    REGEXP_UNICODE_SEQUENCES: /\\u([0-9a-fA-F]{4}|{([0-9a-fA-F]+)})/g,
    REGEXP_IDENTIFIER: regexpIdentifier,
    deserializeUnicodeSequences(string) {
        return string.replace(this.REGEXP_UNICODE_SEQUENCES, (match, s1, s2) => {
            const code = s2 != null ? parseInt(s2, 16) : parseInt(s1, 16);
            return String.fromCodePoint(code);
        });
    },
    isIdentifierOrKeyword(string) {
        return this.REGEXP_IDENTIFIER.test(this.deserializeUnicodeSequences(string));
    },
    isIdentifier(string) {
        if (!this.isIdentifierOrKeyword(string)) {
            return false;
        }
        try {
            // eslint-disable-next-line no-new
            new Function(string, '');
        } catch (e) {
            return false;
        }
        return true;
    },
    canStoreProperties(value) {
        if (value == null) {
            return false;
        }
        const type = typeof value;
        return type === 'object' || type === 'function';
    },
    getType(value) {
        if (value === void 0) {
            return 'undefined';
        } else if (value === null) {
            return 'null';
        } else {
            const type = typeof value;
            if (type === 'function') {
                const name = value.name;
                if (typeof name === 'string' && name.length > 0) {
                    return `[${type} ${name}]`;
                }
                return type;
            } else if (type === 'object') {
                return Object.prototype.toString.call(value);
            } else {
                return type;
            }
        }
    },
    prototypeOf(object, prototype) {
        while (object != null) {
            const objectPrototype = Object.getPrototypeOf(object);
            if (objectPrototype != null && objectPrototype === prototype) {
                return true;
            }
            object = objectPrototype;
        }
        return false;
    },
    instanceOf(object, Class) {
        if (typeof Class !== 'function') {
            return false;
        }
        if (Class.prototype == null) {
            return false;
        }
        return this.prototypeOf(object, Class.prototype);
    },
    getStackTrace(error = null) {
        const prepareStackTraceDescriptor = Object.getOwnPropertyDescriptor(Error, 'prepareStackTrace');
        /* c8 ignore next 3 */
        if (prepareStackTraceDescriptor != null && !prepareStackTraceDescriptor.configurable) {
            return null;
        }
        Object.defineProperty(Error, 'prepareStackTrace', {
            configurable: true,
            writable: true,
            // eslint-disable-next-line handle-callback-err
            value: function (error, callSites) {
                const lines = [];
                /* c8 ignore else */
                if (Array.isArray(callSites)) {
                    for (const callSite of callSites) {
                        lines.push(JSON.stringify(flattenCallSite(callSite)));
                    }
                }
                return lines.join('\n');
            }
        });
        try {
            let stack = '';
            const trace = [];
            if (error != null) {
                stack = error.stack;
            } else {
                const object = {};
                Error.captureStackTrace(object, JavaScript.getStackTrace);
                stack = object.stack;
            }
            if (typeof stack === 'string') {
                stack = stack.split('\n');
                for (let line of stack) {
                    if (!line.startsWith('{')) {
                        return null;
                    }
                    try {
                        line = JSON.parse(line);
                        /* c8 ignore next 3 */
                    } catch {
                        return null;
                    }
                    trace.push(line);
                }
                if (error != null) {
                    // The stack of the error function is probably messed by now,
                    // so we correct it, using the string provided from the call stack.
                    stack = [String(error)];
                    trace.forEach(site => stack.push('    at ' + site.string));
                    error.stack = stack.join('\n');
                }
                return trace;
            }
            return null;
        } finally {
            if (prepareStackTraceDescriptor == null) {
                delete Error.prepareStackTrace;
            } else {
                Object.defineProperty(Error, 'prepareStackTrace', prepareStackTraceDescriptor);
            }
        }
    }
};

Object.defineProperty(JavaScript, 'uninheritable', {
    value: Symbol('uninheritable')
});

function flattenCallSite(callSite) {
    const flatten = Object.create(null);
    for (const name in interfaceCallSite) {
        const getter = interfaceCallSite[name];
        if (typeof callSite[getter] === 'function') {
            flatten[name] = callSite[getter]();
        }
    }
    return flatten;
}

const interfaceCallSite = Object.assign(Object.create(null), {
    columnNumber: 'getColumnNumber',
    evalOrigin: 'getEvalOrigin',
    filename: 'getFileName',
    functionName: 'getFunctionName',
    lineNumber: 'getLineNumber',
    methodName: 'getMethodName',
    position: 'getPosition',
    promiseIndex: 'getPromiseIndex',
    scriptNameOrSourceURL: 'getScriptNameOrSourceURL',
    typeName: 'getTypeName',
    async: 'isAsync',
    eval: 'isEval',
    native: 'isNative',
    promiseAll: 'isPromiseAll',
    topLevel: 'isTopLevel',
    string: 'toString'
});

Object.defineProperties(JavaScript, {
    [Symbol.toStringTag]: {
        configurable: true,
        value: 'JavaScript'
    }
});

export default JavaScript;
