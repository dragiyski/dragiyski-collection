import JavaScript from './javascript.js';

const symbols = {
    iterable: Symbol('iterable')
};

export default function Iterable(iterable) {
    if (!(this instanceof Iterable)) {
        return new Iterable(...arguments);
    }
    if (!Iterable.is(iterable)) {
        throw new TypeError(`Failed to construct 'Iterable': expected argument 1 to be iterable, but iterable[Symbol.iterator] is not a function`);
    }
    this[symbols.iterable] = iterable;
}

Iterable.prototype = Object.create(Object.prototype, {
    constructor: {
        value: Iterable
    },
    concat: {
        configurable: true,
        writable: true,
        value: function concat(...items) {
            return Iterable.concat(this, ...items);
        }
    },
    entries: {
        configurable: true,
        writable: true,
        value: IterableWrapGenerator(function * entries() {
            let index = 0;
            for (const item of this) {
                yield [index++, item];
            }
        })
    },
    every: {
        configurable: true,
        writable: true,
        value: function (callback, thisArg) {
            if (typeof callback !== 'function') {
                throw new TypeError(`Failed to execute 'every' on 'Iterable': expected argument 1 to be a function, got ${JavaScript.getType(callback)}`);
            }
            let index = 0;
            for (const item of this) {
                if (!Function.prototype.call.call(callback, thisArg, item, index++, this)) {
                    return false;
                }
            }
            return true;
        }
    },
    filter: {
        configurable: true,
        writable: true,
        value: IterableWrapGeneratorValidator(function ([callback, ...rest]) {
            if (typeof callback !== 'function') {
                throw new TypeError(`Failed to execute 'filter' on 'Iterable': expected argument 1 to be a function, got ${JavaScript.getType(callback)}`);
            }
            return [callback, ...rest];
        }, function * filter(callback, thisArg) {
            let index = 0;
            for (const item of this) {
                if (Function.prototype.call.call(callback, thisArg, item, index++, this)) {
                    yield item;
                }
            }
        })
    },
    fill: {
        configurable: true,
        writable: true,
        value: IterableWrapGeneratorValidator(function ([fill, start = 0, end = Infinity]) {
            if (typeof start !== 'bigint') {
                start = Number(start);
                if (isNaN(start)) {
                    start = 0;
                }
            }
            if (typeof end !== 'bigint') {
                end = Number(end);
                if (isNaN(end)) {
                    end = 0;
                }
            }
            return [fill, start, end];
        }, function * fill(value, start, end) {
            let counter = 0;
            for (const item of this) {
                const index = counter++;
                if (index >= start && index < end) {
                    yield value;
                } else {
                    yield item;
                }
            }
        })
    },
    find: {
        configurable: true,
        writable: true,
        value: function (callback, thisArg) {
            if (typeof callback !== 'function') {
                throw new TypeError(`Failed to execute 'find' on 'Iterable': expected argument 1 to be a function, got ${JavaScript.getType(callback)}`);
            }
            let index = 0;
            for (const item of this) {
                if (Function.prototype.call.call(callback, thisArg, item, index++, this)) {
                    return item;
                }
            }
        }
    },
    findIndex: {
        configurable: true,
        writable: true,
        value: function (callback, thisArg) {
            if (typeof callback !== 'function') {
                throw new TypeError(`Failed to execute 'findIndex' on 'Iterable': expected argument 1 to be a function, got ${JavaScript.getType(callback)}`);
            }
            let counter = 0;
            for (const item of this) {
                const index = counter++;
                if (Function.prototype.call.call(callback, thisArg, item, index, this)) {
                    return index;
                }
            }
            return -1;
        }
    },
    flat: {
        configurable: true,
        writable: true,
        value: IterableWrapGeneratorValidator(function ([depth]) {
            if (depth === void 0) {
                depth = 1;
            }
            if (typeof depth !== 'bigint') {
                depth = Number(depth);
                if (isNaN(depth)) {
                    depth = 0;
                }
            }
            if (depth < 0) {
                depth = 0;
            }
            return [depth];
        }, function * flat(depth) {
            yield * recursiveFlat(this, depth);
        })
    },
    forEach: {
        configurable: true,
        writable: true,
        value: function (callback, thisArg) {
            if (typeof callback !== 'function') {
                throw new TypeError(`Failed to execute 'forEach' on 'Iterable': expected argument 1 to be a function, got ${JavaScript.getType(callback)}`);
            }
            let index = 0;
            for (const item of this) {
                Function.prototype.call.call(callback, thisArg, item, index++, this);
            }
        }
    },
    indexOf: {
        configurable: true,
        writable: true,
        value: function (value) {
            let counter = 0;
            for (const item of this) {
                const index = counter++;
                if (item === value) {
                    return index;
                }
            }
            return -1;
        }
    },
    join: {
        configurable: true,
        writable: true,
        value: function (glue = ',') {
            const iterator = this[Symbol.iterator]();
            let result = '';
            {
                const iteration = iterator.next();
                if (!iteration.done) {
                    result += iteration.value;
                }
            }
            for (const item of iterator) {
                result += glue;
                result += item;
            }
            return result;
        }
    },
    keys: {
        configurable: true,
        writable: true,
        value: IterableWrapGenerator(function * keys() {
            let index = 0;
            // eslint-disable-next-line no-unused-vars
            for (const item of this) {
                yield index++;
            }
        })
    },
    map: {
        configurable: true,
        writable: true,
        value: IterableWrapGeneratorValidator(function ([callback, ...rest]) {
            if (typeof callback !== 'function') {
                throw new TypeError(`Failed to execute 'map' on 'Iterable': expected argument 1 to be a function, got ${JavaScript.getType(callback)}`);
            }
            return [callback, ...rest];
        }, function * map(callback, thisArg) {
            let index = 0;
            for (const item of this) {
                yield Function.prototype.call.call(callback, thisArg, item, index++, this);
            }
        })
    },
    reduce: {
        configurable: true,
        writable: true,
        value: function (callback, initialValue) {
            if (typeof callback !== 'function') {
                throw new TypeError(`Failed to execute 'reduce' on 'Iterable': expected argument 1 to be a function, got ${JavaScript.getType(callback)}`);
            }
            const iterator = this[Symbol.iterator]();
            let index = 0;
            if (arguments.length < 2) {
                ++index;
                const iteration = iterator.next();
                if (!iteration.done) {
                    initialValue = iteration.value;
                }
            }
            let value = initialValue;
            for (const item of iterator) {
                value = Function.prototype.call.call(callback, undefined, value, item, index++, this);
            }
            return value;
        }
    },
    slice: {
        configurable: true,
        writable: true,
        value: IterableWrapGeneratorValidator(function ([begin = 0, end = Infinity]) {
            if (typeof begin !== 'bigint') {
                begin = Number(begin);
                if (isNaN(begin)) {
                    begin = 0;
                }
            }
            if (typeof end !== 'bigint') {
                end = Number(end);
                if (isNaN(end)) {
                    end = 0;
                }
            }
            return [begin, end];
        }, function * slice(begin, end) {
            let counter = 0;
            for (const item of this) {
                const index = counter++;
                if (index >= begin && index < end) {
                    yield item;
                }
            }
        })
    },
    some: {
        configurable: true,
        writable: true,
        value: function (callback, thisArg) {
            if (typeof callback !== 'function') {
                throw new TypeError(`Failed to execute 'every' on 'Iterable': expected argument 1 to be a function, got ${JavaScript.getType(callback)}`);
            }
            let index = 0;
            for (const item of this) {
                if (Function.prototype.call.call(callback, thisArg, item, index++, this)) {
                    return true;
                }
            }
            return false;
        }
    },
    splice: {
        configurable: true,
        writable: true,
        value: IterableWrapGeneratorValidator(function ([start = 0, deleteCount = 0, insertIterable = []]) {
            if (typeof start !== 'bigint') {
                start = Number(start);
                if (isNaN(start)) {
                    start = 0;
                }
            }
            if (typeof deleteCount !== 'bigint') {
                deleteCount = Number(deleteCount);
                if (isNaN(deleteCount)) {
                    deleteCount = 0;
                }
            }
            if (insertIterable == null) {
                insertIterable = [];
            }
            if (!Iterable.is(insertIterable)) {
                throw new TypeError(`Failed to execute 'splice' on 'Iterable': expected optional argument 2 to be an iterable, but [Symbol.iterator] is not a function`);
            }
            return [start, deleteCount, insertIterable];
        }, function * splice(start, deleteCount, insertIterable) {
            const iterator = this[Symbol.iterator]();
            let index = 0;
            yield * beforeStart();
            skipDeleted();
            yield * insertIterable;
            yield * iterator;

            function * beforeStart() {
                while (index++ < start) {
                    const iteration = iterator.next();
                    if (iteration.done) {
                        return;
                    }
                    yield iteration.value;
                }
            }

            function skipDeleted() {
                while (deleteCount-- > 0) {
                    const iteration = iterator.next();
                    if (iteration.done) {
                        return;
                    }
                }
            }
        })
    },
    values: {
        configurable: true,
        writable: true,
        value: IterableWrapGenerator(function * values() {
            yield * this;
        })
    },
    [Symbol.iterator]: {
        configurable: true,
        writable: true,
        value: function () {
            return this[symbols.iterable][Symbol.iterator]();
        }
    }
});

Object.defineProperties(Iterable, {
    concat: {
        configurable: true,
        writable: true,
        value: IterableWrapGenerator(function * concat(...items) {
            for (const item of items) {
                if (typeof item !== 'string' && item != null && typeof item[Symbol.iterator] === 'function') {
                    yield * item;
                } else {
                    yield item;
                }
            }
        })
    },
    repeat: {
        configurable: true,
        writable: true,
        value: IterableWrapGeneratorValidator(function ([value, count = 1]) {
            if (count == null) {
                count = 1;
            }
            if (typeof count !== 'bigint') {
                count = Number(count);
                if (isNaN(count)) {
                    count = 0;
                }
            }
            return [value, count];
        }, function * repeat(value, count) {
            while (count-- > 0) {
                yield value;
            }
        })
    },
    slice: {
        configurable: true,
        writable: true,
        value: IterableWrapGeneratorValidator(function ([array, begin = 0, end = array.length]) {
            if (!Iterable.is(array) || typeof array.length !== 'number') {
                throw new TypeError(`Failed to execute 'Iterable.reverse': expected argument 1 to be an array-like object`);
            }
            return [
                array,
                this.getReversibleIndex(begin, array),
                this.getReversibleIndex(end, array)
            ];
        }, function * slice(array, begin, end) {
            for (let i = begin; i < end; ++i) {
                yield array[i];
            }
        })
    },
    reverse: {
        configurable: true,
        writable: true,
        value: IterableWrapGeneratorValidator(function ([array, begin = 0, end = array.length]) {
            if (!Iterable.is(array) || typeof array.length !== 'number') {
                throw new TypeError(`Failed to execute 'Iterable.reverse': expected argument 1 to be an array-like object`);
            }
            return [
                array,
                this.getReversibleIndex(begin, array),
                this.getReversibleIndex(end, array)
            ];
        }, function * reverse(array, begin, end) {
            for (let i = end - 1; i >= begin; --i) {
                yield array[i];
            }
        })
    },
    getReversibleIndex: {
        configurable: true,
        writable: true,
        value: function getReversibleIndex(number, array) {
            number = Number(number);
            if (isNaN(number)) {
                return 0;
            }
            if (number > array.length) {
                number = array.length;
            }
            if (number < 0) {
                number = array.length + number;
                if (number < 0) {
                    number = 0;
                }
            }
            return number;
        }
    },
    is: {
        configurable: true,
        writable: true,
        value: function (iterable) {
            return iterable != null && typeof iterable[Symbol.iterator] === 'function';
        }
    }
});

function IterableWrapGenerator(generator) {
    return function () {
        return new Iterable(Function.prototype.apply.call(generator, this, arguments));
    };
}

function IterableWrapGeneratorValidator(validator, generator) {
    return function () {
        return new Iterable(Function.prototype.apply.call(generator, this, validator.call(this, arguments)));
    };
}

function * recursiveFlat(iterable, depth) {
    if (depth < 1) {
        yield * iterable;
        return;
    }
    const one = typeof depth === 'bigint' ? 1n : 1;
    for (const item of iterable) {
        if (item != null && typeof item !== 'string' && typeof item[Symbol.iterator] === 'function') {
            yield * recursiveFlat(item, depth - one);
        } else {
            yield item;
        }
    }
}
