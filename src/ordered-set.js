import Iterable from './iterable.js';

const symbols = {
    set: Symbol('set'),
    list: Symbol('list'),
    keys: Symbol('keys'),
    entries: Symbol('entries')
};

export default class OrderedSet {
    constructor(options) {
        options = { ...options };
        options.indexing ??= true;
        this[symbols.set] = new Set();
        this[symbols.list] = [];
        if (options.indexing) {
            return new Proxy(this, indexHandler);
        }
    }

    replace(item, replacement) {
        if (this[symbols.set].has(item) || this[symbols.set].has(replacement)) {
            let i = 0;
            const length = this[symbols.list].length;
            for (; i < length; ++i) {
                if (this[symbols.list][i] === item || this[symbols.list][i] === replacement) {
                    this[symbols.list][i] = replacement;
                    ++i;
                    break;
                }
            }
            for (; i < length; ++i) {
                if (this[symbols.list][i] === item || this[symbols.list][i] === replacement) {
                    this[symbols.list].splice(i, 1);
                    break;
                }
            }
            this[symbols.set].delete(item);
            this[symbols.set].add(replacement);
        }
        return this;
    }

    remove(item) {
        if (this[symbols.set].has(item)) {
            const index = this[symbols.list].indexOf(item);
            /* istanbul ignore else */
            if (index >= 0) {
                this[symbols.list].splice(index, 1);
            }
            this[symbols.set].delete(item);
        }
        return this;
    }

    clear() {
        this[symbols.set].clear();
        this[symbols.list].length = 0;
        return this;
    }

    contains(item) {
        return this[symbols.set].has(item);
    }

    item(index) {
        if (typeof index === 'symbol') {
            throw new TypeError('Cannot convert a Symbol value to a number');
        }
        index = parseInt(index, 10);
        if (isFinite(index) && index >= 0 && index < this.length) {
            return this[symbols.list][index];
        }
        return null;
    }

    entries() {
        return Iterable(this[symbols.list]).map(key => [key, key])[Symbol.iterator]();
    }

    keys() {
        return this[symbols.list][Symbol.iterator]();
    }

    range(begin = 0, end = this.length) {
        return Iterable.slice(this[symbols.list], begin, end);
    }

    indexOf(item) {
        return this[symbols.list].indexOf(item);
    }

    lastIndexOf(item) {
        return this[symbols.list].lastIndexOf(item);
    }

    isSubsetOf(set) {
        for (const item of this) {
            if (!set.contains(item)) {
                return false;
            }
        }
        return true;
    }

    isSupersetOf(set) {
        return set.isSubsetOf(this);
    }

    clone() {
        const cloning = Object.create(OrderedSet.prototype);
        cloning[symbols.set] = new Set(this[symbols.set]);
        cloning[symbols.list] = [...this[symbols.list]];
        return new Proxy(cloning, indexHandler);
    }

    get length() {
        return this[symbols.list].length;
    }
}

Object.defineProperties(OrderedSet.prototype, {
    [Symbol.iterator]: {
        configurable: true,
        writable: true,
        value: OrderedSet.prototype.keys
    },
    values: {
        configurable: true,
        writable: true,
        value: OrderedSet.prototype.keys
    },
    [Symbol.toStringTag]: {
        configurable: true,
        value: 'OrderedSet'
    }
});

Object.defineProperties(OrderedSet, {
    fromIterable: {
        configurable: true,
        writable: true,
        value: function (list) {
            const set = Object.create(OrderedSet.prototype);
            set[symbols.set] = new Set();
            set[symbols.list] = [...uniqueIterate(list, set[symbols.set])];
            return new Proxy(set, indexHandler);
        }
    },
    from: {
        configurable: true,
        writable: true,
        value: function (...args) {
            const set = Object.create(OrderedSet.prototype);
            set[symbols.set] = new Set();
            set[symbols.list] = [...uniqueIterate(args, set[symbols.set])];
            return new Proxy(set, indexHandler);
        }
    }
});

const indexHandler = {
    has: (target, property) => {
        if (typeof property === 'string') {
            const index = parseInt(property, 10);
            if (isFinite(index) && index >= 0 && index < target.length && index.toString(10) === property) {
                return true;
            }
        }
        return Reflect.has(target, property);
    },
    get: (target, property, receiver) => {
        if (typeof property === 'string') {
            const index = parseInt(property, 10);
            if (isFinite(index) && index >= 0 && index < target.length && index.toString(10) === property) {
                return target.item(index);
            }
        }
        return Reflect.get(target, property, receiver);
    },
    set: (target, property, value, receiver) => {
        if (typeof property === 'string') {
            const index = parseInt(property, 10);
            if (isFinite(index) && index.toString(10) === property && index >= 0) {
                if (index >= target.length) {
                    const error = new RangeError(`Index ${index} out of range`);
                    error.index = index;
                    throw error;
                }
                target[symbols.set].delete(target[symbols.list][index]);
                target[symbols.set].add(target[symbols.list][index] = value);
                return true;
            }
        }
        return Reflect.set(target, property, value, receiver);
    }
};

function * uniqueIterate(list, set) {
    for (const item of list) {
        if (!set.has(item)) {
            set.add(item);
            yield item;
        }
    }
}

Object.defineProperties(OrderedSet.prototype, {
    append: {
        configurable: true,
        writable: true,
        value: insertFactory(
            (array, item) => [array.length - 1, item],
            (array, item) => { array.push(item); }
        )
    },
    prepend: {
        configurable: true,
        writable: true,
        value: insertFactory(
            (array, item) => [0, item],
            (array, item) => { array.unshift(item); }
        )
    },
    insert: {
        configurable: true,
        writable: true,
        value: insertFactory(
            (array, index, item) => [Iterable.getReversibleIndex(index, array), item],
            (array, item, index) => { array.splice(index, 0, item); }
        )
    }
});

function insertFactory(entry, insertDefault) {
    return function () {
        const set = this[symbols.set];
        const array = this[symbols.list];
        const [index, item] = entry(array, ...arguments);
        if (!set.has(item)) {
            // If the item is not in the set, just insert it
            set.add(item);
            insertDefault(array, item, index);
            return this;
        }
        // Otherwise, we might need to reorder, so do a search
        const currentIndex = array.indexOf(item);
        /* c8 ignore next 5 */
        if (currentIndex < 0) {
            // Should never happen, array and set are always in sync
            insertDefault(array, item, index);
            return this;
        }
        // If it happens that the is already at that location, we skip a potentially expensive op
        if (currentIndex !== index) {
            // Native arrays are optimized, so 2 * array.splice better (on average) than one full iteration
            array.splice(currentIndex, 1);
            insertDefault(array, item, index);
        }
        return this;
    };
}

