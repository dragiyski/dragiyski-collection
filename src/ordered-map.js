import Iterable from './iterable.js';

const symbols = {
    map: Symbol('map'),
    list: Symbol('list'),
    keys: Symbol('keys'),
    values: Symbol('values'),
    entries: Symbol('entries')
};

export default class OrderedMap {
    constructor(options) {
        options = { ...options };
        options.indexing ??= true;
        this[symbols.map] = new Map();
        this[symbols.list] = [];
        if (options.indexing) {
            return new Proxy(this, indexHandler);
        }
    }

    get(key) {
        return this[symbols.map].get(key);
    }

    set(key, value) {
        return this.append(key, value);
    }

    remove(item) {
        if (this[symbols.map].has(item)) {
            const index = this[symbols.list].indexOf(item);
            /* c8 ignore else */
            if (index >= 0) {
                this[symbols.list].splice(index, 1);
            }
            this[symbols.map].delete(item);
        }
        return this;
    }

    clear() {
        this[symbols.map].clear();
        this[symbols.list].length = 0;
        return this;
    }

    contains(key) {
        return this[symbols.map].has(key);
    }

    item(index) {
        const i = Number(index);
        if (i === parseInt(index, 10) && i >= 0 && i < this.length) {
            return this[symbols.list][i];
        }
    }

    value(index) {
        const i = Number(index);
        if (i === parseInt(index, 10) && i >= 0 && i < this.length) {
            return this[symbols.map].get(this[symbols.list][i]);
        }
    }

    entries() {
        return Iterable(this[symbols.list]).map(key => [key, this[symbols.map].get(key)])[Symbol.iterator]();
    }

    keys() {
        return this[symbols.list][Symbol.iterator]();
    }

    values() {
        return Iterable(this[symbols.list]).map(key => this[symbols.map].get(key))[Symbol.iterator]();
    }

    indexOf(item) {
        return this[symbols.list].indexOf(item);
    }

    lastIndexOf(item) {
        return this[symbols.list].lastIndexOf(item);
    }

    range(begin = 0, end = this.length) {
        return Iterable.slice(this[symbols.list], begin, end).map(key => [
            key,
            this[symbols.map].get(key)
        ]);
    }

    clone() {
        const cloning = Object.create(OrderedMap.prototype);
        cloning[symbols.map] = new Map(this[symbols.map]);
        cloning[symbols.list] = [...this[symbols.list]];
        return new Proxy(cloning, indexHandler);
    }

    sort(...args) {
        this[symbols.list].sort(...args);
        return this;
    }

    get length() {
        return this[symbols.list].length;
    }
}

Object.defineProperties(OrderedMap.prototype, {
    [Symbol.iterator]: {
        configurable: true,
        writable: true,
        value: OrderedMap.prototype.entries
    },
    [Symbol.toStringTag]: {
        configurable: true,
        value: 'OrderedMap'
    }
});

const indexHandler = {
    has: (target, property) => {
        if (typeof property === 'string') {
            const index = parseInt(property, 10);
            if (isFinite(index) && index >= 0 && index < target.length && index.toString(10) === property) {
                return true;
            }
        }
        return Reflect.has(target, property);
    },
    get: (target, property, receiver) => {
        if (typeof property === 'string') {
            const index = parseInt(property, 10);
            if (isFinite(index) && index >= 0 && index < target.length && index.toString(10) === property) {
                return target[symbols.list][index];
            }
        }
        return Reflect.get(target, property, receiver);
    },
    set: (target, property, value, receiver) => {
        if (typeof property === 'string') {
            const index = parseInt(property, 10);
            if (isFinite(index) && index >= 0 && index.toString(10) === property) {
                if (index >= target.length) {
                    const error = new RangeError(`Index ${index} out of range`);
                    error.index = index;
                    throw error;
                }
                const array = target[symbols.list];
                const map = target[symbols.map];
                const key = array[index];
                const item = map.get(key);
                map.delete(key);
                map.set(value, item);
                array[index] = value;
                return true;
            }
        }
        return Reflect.set(target, property, value, receiver);
    }
};

Object.defineProperties(OrderedMap.prototype, {
    append: {
        configurable: true,
        writable: true,
        value: insertFactory(
            (array, item, value) => [array.length - 1, item, value],
            (array, item) => { array.push(item); }
        )
    },
    prepend: {
        configurable: true,
        writable: true,
        value: insertFactory(
            (array, item, value) => [0, item, value],
            (array, item) => { array.unshift(item); }
        )
    },
    insert: {
        configurable: true,
        writable: true,
        value: insertFactory(
            (array, index, item, value) => [Iterable.getReversibleIndex(index, array), item, value],
            (array, item, index) => { array.splice(index, 0, item); }
        )
    }
});

function insertFactory(entry, insertDefault) {
    return function () {
        const map = this[symbols.map];
        const array = this[symbols.list];
        const [index, item, value] = entry(array, ...arguments);
        if (!map.has(item)) {
            // If the item is not in the set, just insert it
            map.set(item, value);
            insertDefault(array, item, index);
            return this;
        }
        map.set(item, value);
        // Otherwise, we might need to reorder, so do a search
        const currentIndex = array.indexOf(item);
        /* c8 ignore next 5 */
        if (currentIndex < 0) {
            // Should never happen, array and set are always in sync
            insertDefault(array, item, index);
            return this;
        }
        // If it happens that the is already at that location, we skip a potentially expensive op
        if (currentIndex !== index) {
            // Native arrays are optimized, so 2 * array.splice better (on average) than one full iteration
            array.splice(currentIndex, 1);
            insertDefault(array, item, index);
        }
        return this;
    };
}
