const symbols = {
    private: Symbol('private'),
    public: Symbol('public')
};

/**
 * A structure for associating interface objects to their implementation.
 *
 * This structure is useful when an interface must be associated with implementation without adding any additional symbols/properties in the interface.
 * For example, implementation of W3C interfaces that could be accessed by untrusted code (downloaded from the internet) must not have any means to access
 * underlying implementation.
 */
export default class Implementation {
    constructor() {
        this[symbols.private] = new WeakMap();
        this[symbols.public] = Symbol('public');
    }

    getImplementation(object) {
        while (object != null) {
            if (this.hasOwnImplementation(object)) {
                return this.getOwnImplementation(object);
            }
            object = Object.getPrototypeOf(object);
        }
    }

    getInterface(implementation) {
        if (implementation != null) {
            return implementation[this[symbols.public]];
        }
    }

    getOwnImplementation(object) {
        return this[symbols.private].get(object);
    }

    getOwnInterface(implementation) {
        if (this.hasOwnInterface(implementation)) {
            return this.getInterface(implementation);
        }
    }

    hasImplementation(object) {
        while (object != null) {
            if (this.hasOwnImplementation(object)) {
                return true;
            }
            object = Object.getPrototypeOf(object);
        }
        return false;
    }

    hasInterface(implementation) {
        if (implementation == null || typeof implementation === 'boolean' || typeof implementation === 'number' || typeof implementation === 'string' || typeof implementation === 'symbol') {
            return false;
        }
        return this[symbols.public] in implementation;
    }

    hasOwnImplementation(object) {
        return this[symbols.private].has(object);
    }

    hasOwnInterface(implementation) {
        if (implementation == null) {
            return false;
        }
        return Object.hasOwnProperty.call(implementation, this[symbols.public]);
    }

    setImplementation(object, implementation) {
        // In case the implementation is reassigned to a new interface...
        let previousInterface = null;
        // Or in case the interface is reassigned to a new implementation...
        let previousImplementation = null;
        if (this.hasOwnInterface(implementation)) {
            previousInterface = this.getOwnInterface(implementation);
        }
        if (this.hasOwnImplementation(object)) {
            previousImplementation = this.getOwnImplementation(object);
        }
        try {
            implementation[this[symbols.public]] = object;
            this[symbols.private].set(object, implementation);
        } catch (e) {
            if (previousInterface != null) {
                implementation[this[symbols.public]] = previousInterface;
            } else {
                delete implementation[this[symbols.public]];
            }
            if (previousImplementation != null) {
                this[symbols.private].set(object, previousImplementation);
            } else {
                this[symbols.private].delete(object);
            }
            throw e;
        }
        if (previousInterface != null && previousInterface !== object) {
            this[symbols.private].delete(previousInterface);
        }
        if (previousImplementation != null && previousImplementation !== implementation) {
            delete previousImplementation[this[symbols.public]];
        }
        return this;
    }

    removeInterface(object) {
        if (this[symbols.private].has(object)) {
            const implementation = this[symbols.private].get(object);
            delete implementation[this[symbols.public]];
        }
        this[symbols.private].delete(object);
        return this;
    }

    removeImplementation(implementation) {
        if (implementation != null && Object.hasOwnProperty.call(implementation, this[symbols.public])) {
            const object = implementation[this[symbols.public]];
            delete implementation[this[symbols.public]];
            this[symbols.private].delete(object);
        }
        return this;
    }
}

Object.defineProperties(Implementation.prototype, {
    [Symbol.toStringTag]: {
        configurable: true,
        value: 'Implementation'
    }
});
