const mapMixinToImplementation = new WeakMap();
const mapClassToHandle = new WeakMap();
const mapPrototypeToHandle = new WeakMap();
const mapProxyToTarget = new WeakMap();
const mapProxyToHandle = new WeakMap();
const mapInstanceOf = new WeakSet();

const symbols = {
    Class: Symbol('Class'),
    options: Symbol('options')
};

const hasOwnProperty = Object.prototype.hasOwnProperty;
const getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
const getOwnPropertyNames = Object.getOwnPropertyNames;
const getOwnPropertySymbols = Object.getOwnPropertySymbols;
const defineProperty = Object.defineProperty;

/**
 * Mixin class allow a convenient way to create simulated multiple inheritance.
 */
class Mixin extends Function {
    /**
     * Mixin wraps a Class without changing it. Then it can be used to modify another class on a different prototype chain to have the same properties as
     * this class and provides proper instanceof checks.
     *
     * @param {function} Class A class or a function that should be invoked with new to create new object.
     * @param {object} options
     * @param {function} options.root The root class of the prototype chain to be observed.
     * @param {string|function} options.invoke The constructor or constructor-policy to use. Constructor policy can be one of:
     *   * ``"copy"`` - invoke the actual Class with new creating a new object and then copy all own properties to the existing this.
     *   * ``"deny"`` - throw an TypeError.
     *   * ``"call"`` - just invoke the actual Class constructor. It can throw a TypeError, but it is admissible for non-class syntax, although ``new.target``
     * will be lost in this process.
     *   * ``"method"`` - invoke a specific method on the actual Class.prototype.
     *   * ``"static"`` - invoke a specific method on the actual Class.
     *
     * @param {string|Symbol} options.method A name of a method to invoke if option ``invoke`` is a ``"method"`` or ``"static"``.
     */
    constructor(Class, options) {
        if (typeof Class !== 'function' || Class.prototype == null) {
            throw new TypeError(`Failed to create 'Mixin': parameter 1 is not a function/class constructor.`);
        }
        if (mapProxyToTarget.has(Class)) {
            Class = mapProxyToTarget.get(Class);
        }
        if (Function.prototype[Symbol.hasInstance].call(Mixin, Class)) {
            Class = Class.Class;
        }
        if (mapMixinToImplementation.has(Class)) {
            throw new ReferenceError(`Failed to create 'Mixin': parameter 1 is already defined as mixin`);
        }
        options = { ...options };
        options.root ??= Class;
        options.invoke ??= 'copy';
        options.instanceof ??= true;
        if (typeof options.root !== 'function' || options.root.prototype == null) {
            throw new TypeError(`Failed to create 'Mixin': option 'root' is not a function/class contructor`);
        }
        {
            let inPrototypeChain = false;
            for (const classPrototype of getObjectPrototypeChain(Class)) {
                if (classPrototype === options.root) {
                    inPrototypeChain = true;
                    break;
                }
            }
            if (!inPrototypeChain) {
                throw new ReferenceError(`Failed to create 'Mixin': option 'root' is not a equal to or a parent class of parameter 1`);
            }
        }
        if (typeof options.invoke === 'string') {
            if (!(options.invoke in invokePolicy)) {
                throw new TypeError(`Failed to create 'Mixin': option 'invoke' must be either function or one of the: ${Object.keys(invokePolicy).join(', ')}`);
            }
            if (options.invoke === 'method' || options.invoke === 'static') {
                if (typeof options.methodName !== 'string') {
                    throw new TypeError(`Failed to create 'Mixin': when option 'invoke' is 'method' or 'static', option 'methodName' is required`);
                }
                options.invoke = invokePolicy[options.invoke](options.methodName);
            } else {
                options.invoke = invokePolicy[options.invoke];
            }
        }
        if (typeof options.invoke !== 'function') {
            throw new TypeError(`Failed to create 'Mixin': option 'invoke' must be either function or one of the: ${Object.keys(invokePolicy).join(', ')}`);
        }
        if (options.instanceof && !mapInstanceOf.has(options.root)) {
            Object.defineProperty(options.root, Symbol.hasInstance, {
                configurable: true,
                value: hasInstanceOverride(Object.getOwnPropertyDescriptor(options.root, Symbol.hasInstance))
            });
            mapInstanceOf.add(options.root);
        }
        super();
        Object.defineProperties(this, {
            name: {
                configurable: true,
                value: Class.name
            }
        });
        this[symbols.Class] = Class;
        this[symbols.options] = options;
        const handler = new MixinHandler(this, options.invoke);
        const proxy = new Proxy(this, handler);
        mapProxyToHandle.set(proxy, handler);
        mapProxyToTarget.set(proxy, this);
        mapMixinToImplementation.set(Class, proxy);
        return proxy;
    }

    get Class() {
        return this[symbols.Class];
    }

    #interceptStatic(Class) {
        if (mapClassToHandle.has(Class)) {
            const handle = mapClassToHandle.get(Class);
            if (handle.proxy == null) {
                const target = Object.getPrototypeOf(Class);
                const proxy = handle.proxy = new Proxy(target, handle);
                Object.setPrototypeOf(Class, proxy);
            }
            handle.append(this);
        } else {
            const handle = new StaticHandle(this, Class);
            const target = Object.getPrototypeOf(Class);
            const proxy = new Proxy(target, handle);
            handle.proxy = proxy;
            mapProxyToTarget.set(proxy, target);
            mapProxyToHandle.set(proxy, handle);
            mapClassToHandle.set(Class, handle);
            Object.setPrototypeOf(Class, proxy);
        }
    }

    #interceptPrototype(Class) {
        const classPrototype = Class.prototype;
        if (mapPrototypeToHandle.has(classPrototype)) {
            const handle = mapPrototypeToHandle.get(classPrototype);
            if (handle.proxy == null) {
                const target = Object.getPrototypeOf(classPrototype);
                const proxy = handle.proxy = new Proxy(target, handle);
                Object.setPrototypeOf(classPrototype, proxy);
            }
            handle.append(this);
        } else {
            const handle = new PrototypeHandle(this, Class);
            const target = Object.getPrototypeOf(classPrototype);
            const proxy = new Proxy(target, handle);
            handle.proxy = proxy;
            mapProxyToTarget.set(proxy, target);
            mapProxyToHandle.set(proxy, handle);
            mapPrototypeToHandle.set(classPrototype, handle);
            Object.setPrototypeOf(classPrototype, proxy);
        }
    }

    #inheritStatic(Class) {
        inherit.call(this, this[symbols.Class], this[symbols.options].root, Class, mapClassToHandle, StaticHandle, Class);
    }

    #inheritPrototype(Class) {
        inherit.call(this, this[symbols.Class].prototype, this[symbols.options].root.prototype, Class.prototype, mapPrototypeToHandle, PrototypeHandle, Class);
    }

    extends(Class, options) {
        if (typeof Class !== 'function' || Class.prototype == null) {
            throw new TypeError(`Failed to execute 'extends' on 'Mixin': parameter 1 is not a function/class constructor.`);
        }
        if (mapProxyToTarget.has(Class)) {
            Class = mapProxyToTarget.get(Class);
        }
        options = { ...options };
        options.live ??= false;
        const live = {};
        if (typeof options.live === 'object') {
            live.static = Boolean(options.live.static);
            live.prototype = Boolean(options.live.prototype);
        } else {
            live.static = live.prototype = Boolean(options.live);
        }
        let self = this;
        if (mapProxyToTarget.has(self)) {
            self = mapProxyToTarget.get(self);
        }
        if (live.static) {
            self.#interceptStatic(Class);
        } else {
            self.#inheritStatic(Class);
        }
        if (live.prototype) {
            self.#interceptPrototype(Class);
        } else {
            self.#inheritPrototype(Class);
        }
        return this;
    }

    static define() {
        return new Mixin(...arguments);
    }

    toString() {
        return `mixin${this.name.length > 0 ? ` ${this.name}` : ''}`;
    }
}

Object.defineProperties(Mixin, {
    name: {
        value: 'Mixin'
    },
    uninheritables: {
        value: Symbol('uninheritables')
    },
    [Symbol.hasInstance]: {
        configurable: true,
        value: function (instance) {
            if (mapProxyToTarget.has(instance)) {
                instance = mapProxyToTarget.get(instance);
            }
            for (const prototype of getObjectPrototypeChain(instance)) {
                if (mapMixinToImplementation.has(prototype)) {
                    return true;
                }
            }
            return Function.prototype.call.call(Function.prototype[Symbol.hasInstance], this, instance);
        }
    }
});

Object.defineProperties(Mixin.prototype, {
    [Symbol.hasInstance]: {
        configurable: true,
        value: function (instance) {
            let self = this;
            if (mapProxyToTarget.has(this)) {
                self = mapProxyToTarget.get(this);
            }
            if (mapProxyToTarget.has(instance)) {
                instance = mapProxyToTarget.get(instance);
            }
            if (mixinInstanceOf(self.Class, instance)) {
                return true;
            }
            /* c8 ignore next */
            return Function.prototype.call.call(Function.prototype[Symbol.hasInstance], self, instance);
        }
    }
});

function inherit(mixinClass, mixinRoot, target, handleMap, Handle, Class) {
    for (const prototype of getObjectPrototypeChain(mixinClass)) {
        if (mapProxyToTarget.has(prototype)) {
            // Do not read the proxy, (in case live and non-live mixins are mixed).
            continue;
        }
        const uninheritables = hasOwnProperty.call(mixinClass, Mixin.uninheritables) ? mixinClass[Mixin.uninheritables] : null;
        for (const name of getOwnPropertyNames(prototype)) {
            if (name === 'prototype' || name === 'name' || name === 'length' || uninheritables?.[name] || name in target) {
                continue;
            }
            defineProperty(target, name, getOwnPropertyDescriptor(prototype, name));
        }
        for (const symbol of getOwnPropertySymbols(prototype)) {
            if (symbol === Mixin.uninheritables || symbol === Symbol.hasInstance || uninheritables?.[symbol] || symbol in target) {
                continue;
            }
            defineProperty(target, symbol, getOwnPropertyDescriptor(prototype, symbol));
        }
        if (prototype === mixinRoot) {
            break;
        }
    }
    if (!handleMap.has(target)) {
        handleMap.set(target, new Handle(this, Class));
    } else {
        const handle = handleMap.get(target);
        handle.append(this);
    }
}

function hasInstanceOverride(descriptor) {
    let previousHasInstance;
    if (descriptor != null) {
        if (descriptor.value != null) {
            previousHasInstance = function (instance) {
                return Function.prototype.call.call(descriptor.value, this, instance);
            };
        } else if (descriptor.get != null) {
            previousHasInstance = function (instance) {
                return Function.prototype.call.call(Function.prototype.call.call(descriptor.get, this), this, instance);
            };
        }
    } else {
        previousHasInstance = function (instance) {
            return Function.prototype.call.call(Function.prototype[Symbol.hasInstance], this, instance);
        };
    }
    return function (instance) {
        let self = this;
        /* c8 ignore next 3 */
        if (mapProxyToTarget.has(this)) {
            self = mapProxyToTarget.get(this);
        }
        if (mapProxyToTarget.has(instance)) {
            instance = mapProxyToTarget.get(instance);
        }
        if (Function.prototype.call.call(previousHasInstance, self, instance)) {
            return true;
        }
        if (mixinInstanceOf(self, instance)) {
            return true;
        }
        /* c8 ignore next */
        return Function.prototype.call.call(Function.prototype[Symbol.hasInstance], self, instance);
    };
}

function mixinInstanceOf(target, instance) {
    for (const prototype of getInstancePrototypeChain(instance)) {
        if (mapPrototypeToHandle.has(prototype)) {
            const handle = mapPrototypeToHandle.get(prototype);
            for (const mixin of handle.mixins) {
                const RootClass = mixin[symbols.options].root;
                for (const Class of getObjectPrototypeChain(mixin.Class)) {
                    if (Class === target) {
                        return true;
                    }
                    if (Class === RootClass) {
                        break;
                    }
                }
            }
        }
    }
    return false;
}

function * getObjectPrototypeChain(object) {
    while (object != null) {
        yield object;
        object = Object.getPrototypeOf(object);
    }
}

function * getInstancePrototypeChain(object) {
    if (object == null) {
        return;
    }
    yield * getObjectPrototypeChain(Object.getPrototypeOf(object));
}

const invokePolicy = Object.assign(Object.create(null), {
    deny(target, thisArg) {
        const handle = findHandleForMixin(target, thisArg);
        if (handle == null) {
            throw new TypeError(`Failed to execute a mixin on '${target.name}': the function is called with incompatible 'this'`);
        }
        throw new TypeError(`Mixin's class constructor '${target[symbols.Class].name}' cannot be invoked without 'new'`);
    },
    call(target, thisArg, argList) {
        const handle = findHandleForMixin(target, thisArg);
        if (handle == null) {
            throw new TypeError(`Failed to execute a mixin on '${target.name}': the function is called with incompatible 'this'`);
        }
        Reflect.apply(target[symbols.Class], thisArg, argList);
        return thisArg;
    },
    method(name) {
        return function (target, thisArg, argList) {
            const handle = findHandleForMixin(target, thisArg);
            if (handle == null) {
                throw new TypeError(`Failed to execute a mixin on '${target.name}': the function is called with incompatible 'this'`);
            }
            const callback = target[symbols.Class].prototype[name];
            if (typeof callback !== 'function') {
                throw new ReferenceError(`Mixin's class property '${target[symbols.Class].name}.prototype.${String(name)}' is not a function`);
            }
            return Reflect.apply(callback, thisArg, argList);
        };
    },
    static(name) {
        return function (target, thisArg, argList) {
            const handle = findHandleForMixin(target, thisArg);
            if (handle == null) {
                throw new TypeError(`Failed to execute a mixin on '${target.name}': the function is called with incompatible 'this'`);
            }
            const callback = target[symbols.Class][name];
            if (typeof callback !== 'function') {
                throw new ReferenceError(`Mixin's class property '${target[symbols.Class].name}.${String(name)}' is not a function`);
            }
            return Reflect.apply(callback, thisArg, argList);
        };
    },
    copy(target, thisArg, argList) {
        const handle = findHandleForMixin(target, thisArg);
        if (handle == null) {
            throw new TypeError(`Failed to execute a mixin on '${target.name}': the function is called with incompatible 'this'`);
        }
        // TODO: Find the correct new.target here!
        const source = Reflect.construct(target[symbols.Class], argList, handle.Class);
        for (const name of getOwnPropertyNames(source)) {
            defineProperty(thisArg, name, Object.getOwnPropertyDescriptor(source, name));
        }
        for (const symbol of getOwnPropertySymbols(source)) {
            defineProperty(thisArg, symbol, Object.getOwnPropertyDescriptor(source, symbol));
        }
        return thisArg;
    }
});

function findHandleForMixin(target, thisArg) {
    for (const prototype of getInstancePrototypeChain(thisArg)) {
        if (mapProxyToHandle.has(prototype)) {
            const maybeHandle = mapProxyToHandle.get(prototype);
            if (maybeHandle instanceof PrototypeHandle && maybeHandle.mixins.indexOf(target) >= 0) {
                return maybeHandle;
            }
        } else if (mapPrototypeToHandle.has(prototype)) {
            const maybeHandle = mapPrototypeToHandle.get(prototype);
            if (maybeHandle instanceof PrototypeHandle && maybeHandle.mixins.indexOf(target) >= 0) {
                return maybeHandle;
            }
        }
    }
}

/**
 * A handler for proxies that lies on the prototype chain of the extended class right before the extended class.
 */
class Handle {
    constructor(mixin, Class) {
        this.mixins = [mixin];
        this.Class = Class;
    }

    getPrototypeOf(target) {
        return target;
    }

    append(mixin) {
        if (this.mixins.indexOf(mixin) < 0) {
            this.mixins.push(mixin);
        }
        return this;
    }

    isUninheritable(target, property) {
        if (hasOwnProperty.call(target, Mixin.uninheritables)) {
            const uninheritables = target[Mixin.uninheritables];
            return uninheritables ?? uninheritables[property];
        }
    }

    isSpecialMixinProperty(property) {
        return property === Mixin.uninheritables || property === Symbol.hasInstance;
    }

    lookupHandle(property) {
        if (this.isSpecialMixinProperty(property)) {
            return false;
        }
        for (const mixin of this.mixins) {
            let rootReached = false;
            for (const object of getObjectPrototypeChain(this.getTargetForClass(mixin.Class))) {
                if (rootReached) {
                    break;
                }
                if (mapProxyToTarget.has(object)) {
                    continue;
                }
                if (object === this.getTargetForClass(mixin[symbols.options].root)) {
                    rootReached = true;
                }
                if (this.isUninheritable(object, property)) {
                    continue;
                }
                if (hasOwnProperty.call(object, property)) {
                    return object;
                }
            }
        }
        return null;
    }

    lookup(target, property) {
        // Search the prototype chain, without searching any mixins...
        for (const prototype of getObjectPrototypeChain(target)) {
            if (mapProxyToTarget.has(prototype)) {
                continue;
            }
            if (hasOwnProperty.call(prototype, property)) {
                return prototype;
            }
        }
        // Search the current handler
        {
            const maybeTarget = this.lookupHandle(property);
            if (maybeTarget != null) {
                return maybeTarget;
            }
        }
        // Search the prototype chain for any property from a mixin...
        for (const prototype of getObjectPrototypeChain(target)) {
            if (mapProxyToHandle.has(prototype)) {
                const handle = mapProxyToHandle.get(prototype);
                if (handle instanceof this.constructor) {
                    const maybeTarget = handle.lookupHandle(property);
                    if (maybeTarget != null) {
                        return maybeTarget;
                    }
                }
            }
        }
        return null;
    }

    has(target, property) {
        return this.lookup(target, property) != null;
    }

    get(target, property, receiver) {
        const actualTarget = this.lookup(target, property);
        if (actualTarget != null) {
            return Reflect.get(actualTarget, property, receiver);
        }
    }
}

class StaticHandle extends Handle {
    isSpecialMixinProperty(property) {
        return property === 'name' || property === 'length' || property === 'prototype' || super.isSpecialMixinProperty(property);
    }

    getTargetForClass(Class) {
        return Class;
    }
}

class PrototypeHandle extends Handle {
    isSpecialMixinProperty(property) {
        return property === 'constructor' || super.isSpecialMixinProperty(property);
    }

    getTargetForClass(Class) {
        return Class.prototype;
    }
}

class MixinHandler {
    constructor(mixin, policy) {
        this.mixin = mixin;
        Object.defineProperty(this, 'apply', {
            configurable: true,
            writable: true,
            value: policy
        });
    }
}

const moduleHandler = {
    apply(target, thisArg, [Class]) {
        if (typeof Class !== 'function') {
            return;
        }
        if (mapProxyToTarget.has(Class)) {
            Class = mapProxyToTarget.get(Class);
        }
        if (mapMixinToImplementation.has(Class)) {
            return mapMixinToImplementation.get(Class);
        }
    }
};

const proxy = new Proxy(Mixin, moduleHandler);
mapProxyToTarget.set(proxy, Mixin);
mapProxyToHandle.set(proxy, moduleHandler);
export default proxy;
