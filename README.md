This module contains extended collection in addition to existing ECMAScript collections:

* ``OrderedSet`` is a collection similar to ``Set``, but with a guaranteed order and indexing.
* ``OrderedMap`` is a collection similar to ``Map``, but with a guaranteed order and indexing.
* ``Implementation`` is a collection that can associate interfaces to their relevant implementation.
The collection is similar to ``Symbol`` primitive, but it does not modify the interface objects -
no string or symbol properties are added to that object. A symbol is added to the implementation object
to link back the interface.
* ``WeakOrderedSet`` is a collection loosly similar to ``OrderedSet`` and ``Array``, but objects
assigned to this collection are hold by weak references and can be garbage collected. Like ``WeakSet``
the collection can only store objects (not primitives), but maintains the order of the objects inside.
Indexing is not supported for this collection.

### Installation

To install this module, run:

```
npm install @dragiyski/collection
```

### Usage

To use this module, use either shorthand syntax:
```javascript
const { OrderedSet, OrderedMap } = require('@dragiyski/collection');
```
or include specific collection by filename:
```javascript
const OrderedSet = require('@dragiyski/collection/ordered-set');
```

All filenames use dash-case.
