import { assert, expect } from 'chai';
import { suite, test } from 'mocha';
import Iterable from '../src/iterable.js';

suite('Iterable', function () {
    test('class', function () {
        assert(typeof Iterable === 'function' && Iterable.prototype != null, `Iterable should be a class/constructor function`);
    });
    test('new', function () {
        expect(() => {
            // eslint-disable-next-line no-new
            new Iterable(null);
        }).to.throw(TypeError);
        expect(() => {
            // eslint-disable-next-line no-new
            new Iterable(undefined);
        }).to.throw(TypeError);
        expect(() => {
            // eslint-disable-next-line no-new
            new Iterable(2.3);
        }).to.throw(TypeError);
        expect(() => {
            // eslint-disable-next-line no-new
            new Iterable(Symbol('test'));
        }).to.throw(TypeError);
        expect(() => {
            // eslint-disable-next-line no-new
            new Iterable(true);
        }).to.throw(TypeError);
        expect(() => {
            // eslint-disable-next-line no-new
            new Iterable(false);
        }).to.throw(TypeError);
        let iterable;
        expect(() => {
            iterable = Iterable([]);
        }).to.not.throw();
        assert(iterable instanceof Iterable, `iterable instanceof Iterable`);
    });
    test('map(), filter(): iteration order', function () {
        const items = [[], [], []];
        for (let i = 0; i < 10; ++i) {
            items[0].push({
                index: i,
                group: 0
            });
            items[1].push({
                index: i,
                group: 1
            });
            items[2].push({
                index: i,
                group: 2
            });
        }
        const begin = 4;
        const end = 7;
        const calls = [];
        const result = Iterable(items[0])
            .map(item => {
                const result = items[1][item.index];
                calls.push({
                    job: 0,
                    item,
                    return: result
                });
                return result;
            }).filter(item => {
                const result = item.index >= begin && item.index < end;
                calls.push({
                    job: 1,
                    item,
                    return: result
                });
                return result;
            }).map(item => {
                const result = items[2][item.index];
                calls.push({
                    job: 2,
                    item,
                    return: result
                });
                return result;
            });
        let index = 0;
        for (const item of result) {
            const itemIndex = index++;
            expect(item).to.be.equal(items[2][begin + itemIndex]);
        }
        for (let i = 0; i < calls.length; ++i) {
            const call = calls[i];
            let expectJob = -1;
            let expectItem = null;
            let expectResult = null;
            if (i < begin * 2) {
                const itemIndex = Math.floor(i / 2);
                expectJob = i % 2;
                expectItem = items[expectJob][itemIndex];
                expectResult = expectJob ? false : items[1][itemIndex];
            } else if (i < begin * 2 + (end - begin) * 3) {
                const relativeIndex = i - begin * 2;
                const itemIndex = begin + Math.floor(relativeIndex / 3);
                expectJob = relativeIndex % 3;
                expectItem = expectJob ? items[1][itemIndex] : items[0][itemIndex];
                if (expectJob === 0) {
                    expectResult = items[1][itemIndex];
                } else if (expectJob === 1) {
                    expectResult = true;
                } else if (expectJob === 2) {
                    expectResult = items[2][itemIndex];
                }
            } else {
                const relativeIndex = i - begin * 2 - (end - begin) * 3;
                const itemIndex = end + Math.floor(relativeIndex / 2);
                expectJob = relativeIndex % 2;
                expectItem = items[expectJob][itemIndex];
                expectResult = expectJob ? false : items[1][itemIndex];
            }
            expect(call.job).to.be.equal(expectJob);
            expect(call.item).to.be.equal(expectItem);
            expect(call.return).to.be.equal(expectResult);
        }
    });
    test('concat()', function () {
        const items = [];
        for (let i = 0; i < 5; ++i) {
            if (i === 3) {
                items.push({});
                continue;
            }
            const array = [];
            for (let i = 0; i < 10; ++i) {
                array.push({});
            }
            items.push(array);
        }
        const first = [1, 2, 3];
        const concatAll = [].concat(...items);
        const concatFirst = first.slice().concat(...items);
        const instance = Iterable(first);
        let counter = 0;
        for (const item of instance.concat(...items)) {
            const index = counter++;
            expect(item).to.be.equal(concatFirst[index]);
        }
        counter = 0;
        for (const item of Iterable.concat(...items)) {
            const index = counter++;
            expect(item).to.be.equal(concatAll[index]);
        }
    });
    test('entries()', function () {
        const array = [];
        array[3] = {};
        array[6] = {};
        array[8] = {};
        let counter = 0;
        for (const item of Iterable(array).entries()) {
            const expectedIndex = counter++;
            assert(Array.isArray(item), `Array.isArray(item)`);
            expect(item.length).to.be.equal(2);
            const actualIndex = item[0];
            const value = item[1];
            expect(actualIndex).to.be.equal(expectedIndex);
            expect(value).to.be.equal(array[actualIndex]);
        }
    });
    test('every()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ value: true });
        }
        const instance = Iterable(items);
        for (const item of notFunction) {
            expect(() => { instance.every(item); }).to.throw(TypeError);
        }
        const o = {};
        let calls = [];
        let result = instance.every(function (item, index) {
            calls.push({
                this: this,
                item,
                index
            });
            return item.value;
        }, o);
        expect(result).to.be.equal(true);
        expect(calls.length).to.be.equal(items.length);
        for (let i = 0; i < calls.length; ++i) {
            const call = calls[i];
            expect(call.this).to.be.equal(o);
            expect(call.index).to.be.equal(i);
            expect(call.item).to.be.equal(items[i]);
        }
        (function () {
            'use strict';
            const calls = [];
            instance.every(function (item, index) {
                calls.push({
                    this: this,
                    item,
                    index
                });
                return item.value;
            });
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                assert.strictEqual(call.this, undefined);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
            }
        })();
        calls = [];
        items[5].value = false;
        result = instance.every(function (item, index) {
            calls.push({
                this: this,
                item,
                index
            });
            return item.value;
        }, o);
        expect(result).to.be.equal(false);
        expect(calls.length).to.be.equal(6);
        for (let i = 0; i < calls.length; ++i) {
            const call = calls[i];
            expect(call.this).to.be.equal(o);
            expect(call.index).to.be.equal(i);
            expect(call.item).to.be.equal(items[i]);
        }
    });
    test('filter()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ value: true });
        }
        const instance = Iterable(items);
        for (const item of notFunction) {
            expect(() => { instance.filter(item); }, `filter() must throw if callback argument is ${String(item)}`).to.throw(TypeError);
        }
        {
            const o = {};
            const calls = [];
            const result = instance.filter(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            }, o);
            const mapped = [...result];
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(o);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
            assert.instanceOf(result, Iterable);
            expect(mapped.length).to.be.equal(items.length);
            for (let i = 0; i < mapped.length; ++i) {
                expect(mapped[i]).to.be.equal(items[i]);
            }
        }
        (function () {
            'use strict';
            const calls = [];
            instance.every(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            });
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                assert.strictEqual(call.this, undefined);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
        })();
        {
            const o = {};
            const calls = [];
            items[5].value = false;
            const result = instance.filter(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            }, o);
            const mapped = [...result];
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(o);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
            assert.instanceOf(result, Iterable);
            expect(mapped.length).to.be.equal(items.length - 1);
            for (let i = 0; i < mapped.length; ++i) {
                expect(mapped[i]).to.be.equal(items[i < 5 ? i : (i + 1)]);
            }
        }
    });
    test('fill()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ original: true });
        }
        const original = items.slice(0);
        const filler = { fill: true };
        const instance = Iterable(items);
        const testFill = (...args) => {
            const expected = items.slice().fill(...args);
            const actual = [...Iterable(items).fill(...args)];
            expect(actual.length).to.be.equal(expected.length);
            expect(actual.length).to.be.equal(items.length);
            for (let i = 0; i < items.length; ++i) {
                expect(expected[i]).to.be.equal(actual[i]);
                expect(items[i]).to.be.equal(original[i]);
            }
        };
        testFill(filler, 3, 6);
        testFill(filler, 3, 12);
        testFill(filler, 3);
        testFill(filler, null, null);
        testFill(filler, null, 3);
        testFill(filler, 3, null);
        testFill(filler);
        testFill();
        testFill(filler, '4', '7');
        testFill(filler, '4');
        instance.fill(filler, 'test', 'test');
        instance.fill(filler, '4', 'test');
        instance.fill(filler, 'test');
        {
            const expected = items.slice().fill(filler, 3, 7);
            const actual = [...Iterable(items).fill(filler, 3n, 7n)];
            expect(actual.length).to.be.equal(expected.length);
            expect(actual.length).to.be.equal(items.length);
            for (let i = 0; i < items.length; ++i) {
                expect(expected[i]).to.be.equal(actual[i]);
                expect(items[i]).to.be.equal(original[i]);
            }
        }
    });
    test('find()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ value: false });
        }
        const instance = Iterable(items);
        for (const item of notFunction) {
            expect(() => { instance.find(item); }, `find() must throw if callback argument is ${String(item)}`).to.throw(TypeError);
        }
        {
            const o = {};
            const calls = [];
            const result = instance.find(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            }, o);
            expect(calls.length).to.be.equal(items.length);
            expect(result).to.be.equal(undefined);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(o);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
        }
        (function () {
            'use strict';
            const calls = [];
            instance.find(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            });
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                assert.strictEqual(call.this, undefined);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
        })();
        items[4].value = true;
        {
            const o = {};
            const calls = [];
            const result = instance.find(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            }, o);
            expect(calls.length).to.be.equal(5);
            expect(result).to.be.equal(items[4]);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(o);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
        }
    });
    test('findIndex()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ value: false });
        }
        const instance = Iterable(items);
        for (const item of notFunction) {
            expect(() => { instance.findIndex(item); }, `find() must throw if callback argument is ${String(item)}`).to.throw(TypeError);
        }
        {
            const o = {};
            const calls = [];
            const result = instance.findIndex(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            }, o);
            expect(calls.length).to.be.equal(items.length);
            expect(result).to.be.equal(-1);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(o);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
        }
        (function () {
            'use strict';
            const calls = [];
            instance.findIndex(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            });
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                assert.strictEqual(call.this, undefined);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
        })();
        items[4].value = true;
        {
            const o = {};
            const calls = [];
            const result = instance.findIndex(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            }, o);
            expect(calls.length).to.be.equal(5);
            expect(result).to.be.equal(4);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(o);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
        }
    });
    test('flat()', function () {
        testFlat(5, 1);
        testFlat(10, 3);
        testFlat(10, -3);
        testFlat(10, 10);
        testFlat(10, 20);
        testFlat(10, Infinity);
        testFlat(4, undefined);
        testFlat(4, null);
        testFlat(4, 'test');
        testFlat(4, true);
        testFlat(4, false);
        testFlat(4, {});

        {
            const source = getNested(10);
            const expected = source.slice().flat(3);
            const iterable = Iterable(source).flat(3n);
            const actual = [];
            for (const item of iterable) {
                actual.push(item);
            }
            expect(expected.length).to.be.equal(actual.length);
            for (let i = 0; i < actual.length; ++i) {
                expect(expected[i]).to.be.equal(actual[i]);
            }
        }

        function testFlat(itemDepth, flatDepth) {
            const source = getNested(itemDepth);
            const expected = source.slice().flat(flatDepth);
            const iterable = Iterable(source).flat(flatDepth);
            const actual = [];
            for (const item of iterable) {
                actual.push(item);
            }
            expect(expected.length).to.be.equal(actual.length);
            for (let i = 0; i < actual.length; ++i) {
                expect(expected[i]).to.be.equal(actual[i]);
            }
        }

        function getNested(depth) {
            let index = 0;
            const array = [{ index: index++ }];
            for (let i = 0; i < depth; ++i) {
                let k = i;
                let o = { index: index++ };
                while (k-- >= 0) {
                    o = [o];
                }
                array.push(o);
            }
            return array;
        }
    });
    test('forEach()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({});
        }
        const instance = Iterable(items);
        for (const item of notFunction) {
            expect(() => { instance.forEach(item); }).to.throw(TypeError);
        }
        {
            const o = {};
            const calls = [];
            const result = instance.forEach(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            }, o);
            expect(result).to.be.equal(undefined);
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(o);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
        }
        (function () {
            'use strict';
            const calls = [];
            const result = instance.forEach(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return item.value;
            });
            expect(result).to.be.equal(undefined);
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(undefined);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
        })();
    });
    test('indexOf()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({});
        }
        const other = {};
        const instance = Iterable(items);
        for (let i = 0; i < items.length; ++i) {
            expect(instance.indexOf(items[i])).to.be.equal(i);
        }
        expect(instance.indexOf(other)).to.be.equal(-1);
    });
    test('join()', function () {
        const items = [];
        for (let i = 0; i < 26; ++i) {
            items.push(String.fromCharCode(0x61 + i));
        }
        const instance = Iterable(items);
        expect(instance.join()).to.be.equal(items.join());
        expect(instance.join(' ')).to.be.equal(items.join(' '));
        expect(instance.join('')).to.be.equal(items.join(''));
        items.push(Symbol('test'));
        expect(() => { instance.join(); }).to.throw(TypeError);
        expect(Iterable(['test']).join()).to.be.equal('test');
        expect(Iterable([]).join()).to.be.equal('');
    });
    test('keys()', function () {
        const items = ['a', 'b', 'c', 'd', 'e', 'f'];
        let counter = 0;
        const instance = Iterable(items);
        for (const item of instance.keys()) {
            const index = counter++;
            expect(item).to.be.equal(index);
        }
    });
    test('map()', function () {
        const items = [];
        const values = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ item: i });
            values.push({ value: i });
        }
        const instance = Iterable(items);
        for (const item of notFunction) {
            expect(() => { instance.map(item); }, `map() must throw if callback argument is ${String(item)}`).to.throw(TypeError);
        }
        {
            const o = {};
            const calls = [];
            const result = instance.map(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return values[index];
            }, o);
            assert.instanceOf(result, Iterable);
            const mapped = [...result];
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(o);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
            expect(mapped.length).to.be.equal(items.length);
            for (let i = 0; i < mapped.length; ++i) {
                expect(mapped[i]).to.be.equal(values[i]);
            }
        }
        (function () {
            'use strict';
            const calls = [];
            const result = instance.map(function (item, index, iterable) {
                calls.push({
                    this: this,
                    item,
                    index,
                    iterable
                });
                return values[index];
            });
            assert.instanceOf(result, Iterable);
            const mapped = [...result];
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(undefined);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
            expect(mapped.length).to.be.equal(items.length);
            for (let i = 0; i < mapped.length; ++i) {
                expect(mapped[i]).to.be.equal(values[i]);
            }
        })();
    });
    test('map()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push(i);
        }
        const instance = Iterable(items);
        const sum = items.reduce((prev, current) => prev + current);
        for (const item of notFunction) {
            expect(() => { instance.reduce(item); }, `reduce() must throw if callback argument is ${String(item)}`).to.throw(TypeError);
        }
        {
            const calls = [];
            const result = instance.reduce(function (prev, item, index, iterable) {
                calls.push({
                    this: this,
                    prev,
                    item,
                    index,
                    iterable
                });
                return prev + item;
            });
            expect(calls.length).to.be.equal(items.length - 1);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(undefined);
                expect(call.index).to.be.equal(i + 1);
                expect(call.item).to.be.equal(items[i + 1]);
                expect(call.iterable).to.be.equal(instance);
            }
            expect(result).to.be.equal(sum);
        }
        {
            const calls = [];
            const result = instance.reduce(function (prev, item, index, iterable) {
                calls.push({
                    this: this,
                    prev,
                    item,
                    index,
                    iterable
                });
                return prev + item;
            }, 42);
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                expect(call.this).to.be.equal(undefined);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
                expect(call.iterable).to.be.equal(instance);
            }
            expect(result).to.be.equal(sum + 42);
        }
        {
            let called = false;
            expect(Iterable([]).reduce(() => { called = true; }, 5)).to.be.equal(5);
            expect(called).to.be.equal(false);
        }
        {
            let called = false;
            expect(Iterable([]).reduce(() => { called = true; })).to.be.equal(undefined);
            expect(called).to.be.equal(false);
        }
    });
    test('slice()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ index: i });
        }
        {
            const expected = items.slice();
            const instance = Iterable(items).slice();
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice(3, 7);
            const instance = Iterable(items).slice(3, 7);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice(3, 7);
            const instance = Iterable(items).slice(3n, 7n);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice(3);
            const instance = Iterable(items).slice(3);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            const instance = Iterable(items).slice('not-a-number');
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const instance = Iterable(items).slice(0, 'not-a-number');
            const result = [...instance];
            expect(result.length).to.be.equal(0);
        }
    });
    test('some()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ value: false });
        }
        const instance = Iterable(items);
        for (const item of notFunction) {
            expect(() => { instance.some(item); }).to.throw(TypeError);
        }
        const o = {};
        let calls = [];
        let result = instance.some(function (item, index) {
            calls.push({
                this: this,
                item,
                index
            });
            return item.value;
        }, o);
        expect(result).to.be.equal(false);
        expect(calls.length).to.be.equal(items.length);
        for (let i = 0; i < calls.length; ++i) {
            const call = calls[i];
            expect(call.this).to.be.equal(o);
            expect(call.index).to.be.equal(i);
            expect(call.item).to.be.equal(items[i]);
        }
        (function () {
            'use strict';
            const calls = [];
            instance.some(function (item, index) {
                calls.push({
                    this: this,
                    item,
                    index
                });
                return item.value;
            });
            expect(calls.length).to.be.equal(items.length);
            for (let i = 0; i < calls.length; ++i) {
                const call = calls[i];
                assert.strictEqual(call.this, undefined);
                expect(call.index).to.be.equal(i);
                expect(call.item).to.be.equal(items[i]);
            }
        })();
        calls = [];
        items[5].value = true;
        result = instance.some(function (item, index) {
            calls.push({
                this: this,
                item,
                index
            });
            return item.value;
        }, o);
        expect(result).to.be.equal(true);
        expect(calls.length).to.be.equal(6);
        for (let i = 0; i < calls.length; ++i) {
            const call = calls[i];
            expect(call.this).to.be.equal(o);
            expect(call.index).to.be.equal(i);
            expect(call.item).to.be.equal(items[i]);
        }
    });
    test('splice()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ index: i });
        }
        const inserted = [];
        for (let i = 0; i < 5; ++i) {
            inserted.push({ inserted: i });
        }
        {
            const expected = items.slice();
            expected.splice(2, 3, ...inserted);
            const instance = Iterable(items).splice(2, 3, inserted);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            expected.splice(2, 3, ...inserted);
            const instance = Iterable(items).splice(2n, 3n, inserted);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            expected.splice(0, 3, ...inserted);
            const instance = Iterable(items).splice('not-a-number', '3', inserted);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            expected.splice(4, 0, ...inserted);
            const instance = Iterable(items).splice({ toString: () => 4 }, 'not-a-number', inserted);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            const instance = Iterable(items).splice();
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            const instance = Iterable(items).splice(3);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            expected.splice(4, 2);
            const instance = Iterable(items).splice(4, 2);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            expected.splice(4, 2);
            const instance = Iterable(items).splice(4, 2, null);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            expected.splice(50, 2, ...inserted);
            const instance = Iterable(items).splice(50, 2, inserted);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            expected.splice(2, 50, ...inserted);
            const instance = Iterable(items).splice(2, 50, inserted);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        expect(() => { Iterable(items).splice(Symbol('test')); }).to.throw(TypeError);
        expect(() => { Iterable(items).splice(0, Symbol('test')); }).to.throw(TypeError);
        expect(() => { Iterable(items).splice(0, 0, 42); }).to.throw(TypeError);
    });
    test('values()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ index: i });
        }
        const instance = Iterable(items).values();
        const result = [...instance];
        expect(result.length).to.be.equal(items.length);
        for (let i = 0; i < result.length; ++i) {
            expect(result[i]).to.be.equal(items[i]);
        }
    });
    test('static repeat()', function () {
        const value = { foo: 'bar' };
        {
            const instance = Iterable.repeat(value, 10);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(10);
            for (let i = 0; i < 10; ++i) {
                expect(result[i]).to.be.equal(value);
            }
        }
        {
            const instance = Iterable.repeat(value, 10n);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(10);
            for (let i = 0; i < 10; ++i) {
                expect(result[i]).to.be.equal(value);
            }
        }
        {
            const instance = Iterable.repeat(value);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(1);
            expect(result[0]).to.be.equal(value);
        }
        {
            const instance = Iterable.repeat(value, 0);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(0);
        }
        {
            const instance = Iterable.repeat(value, 'not-a-number');
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(0);
        }
        {
            const instance = Iterable.repeat(value, null);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(1);
            expect(result[0]).to.be.equal(value);
        }
        {
            const instance = Iterable.repeat(value, undefined);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(1);
            expect(result[0]).to.be.equal(value);
        }
        expect(() => { Iterable.repeat(value, Symbol('test')); }).to.throw(TypeError);
    });
    test('static slice()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ index: i });
        }
        {
            const expected = items.slice();
            const instance = Iterable.slice(items);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice(3, 7);
            const instance = Iterable.slice(items, 3, 7);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice(3);
            const instance = Iterable.slice(items, 3);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice();
            const instance = Iterable.slice(items, 'not-a-number');
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const instance = Iterable.slice(items, 0, 'not-a-number');
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(0);
        }
        expect(() => { Iterable.slice({}); }).to.throw(TypeError);
        expect(() => { Iterable.slice([], Symbol('test')); }).to.throw(TypeError);
        expect(() => { Iterable.slice([], 0, Symbol('test')); }).to.throw(TypeError);
    });
    test('reverse()', function () {
        const items = [];
        for (let i = 0; i < 10; ++i) {
            items.push({ index: i });
        }
        {
            const expected = items.slice().reverse();
            const instance = Iterable.reverse(items);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice(3).reverse();
            const instance = Iterable.reverse(items, 3);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        {
            const expected = items.slice(3, 7).reverse();
            const instance = Iterable.reverse(items, 3, 7);
            assert.instanceOf(instance, Iterable);
            const result = [...instance];
            expect(result.length).to.be.equal(expected.length);
            for (let i = 0; i < result.length; ++i) {
                expect(result[i]).to.be.equal(expected[i]);
            }
        }
        expect(() => { Iterable.reverse({}); }).to.throw(TypeError);
        expect(() => { Iterable.reverse([], Symbol('test')); }).to.throw(TypeError);
        expect(() => { Iterable.reverse([], 0, Symbol('test')); }).to.throw(TypeError);
    });
});

const notFunction = [
    undefined,
    null,
    false,
    true,
    5,
    2.5,
    'test',
    Symbol('test'),
    {}
];
