import { assert } from 'chai';
import { suite, test } from 'mocha';
import Mixin from '../src/mixin.js';
import { generator, compare } from './helper/class-generator.js';

suite('Mixin', function () {
    test('.define()', function () {
        const f = function () {};
        f.prototype = null;
        assert.throws(() => { Mixin.define(); }, TypeError);
        assert.throws(() => { Mixin.define(undefined); }, TypeError);
        assert.throws(() => { Mixin.define(null); }, TypeError);
        assert.throws(() => { Mixin.define(false); }, TypeError);
        assert.throws(() => { Mixin.define(true); }, TypeError);
        assert.throws(() => { Mixin.define(7); }, TypeError);
        assert.throws(() => { Mixin.define(3.14); }, TypeError);
        assert.throws(() => { Mixin.define(5n); }, TypeError);
        assert.throws(() => { Mixin.define('test'); }, TypeError);
        assert.throws(() => { Mixin.define(Symbol('test')); }, TypeError);
        assert.throws(() => { Mixin.define({}); }, TypeError);
        assert.throws(() => { Mixin.define(f); }, TypeError);

        class Mixin1 {}

        assert.doesNotThrow(() => { Mixin.define(Mixin1); });
        assert.throws(() => { Mixin.define(Mixin1); }, ReferenceError);
        assert.throws(() => { Mixin.define(Mixin(Mixin1)); }, ReferenceError);

        class Related2 {}

        class Mixin2 extends Related2 {}

        class Unrelated2 {}

        assert.throws(() => { Mixin.define(Mixin2, { root: false }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin2, { root: true }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin2, { root: 7 }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin2, { root: 3.14 }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin2, { root: 5n }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin2, { root: 'test' }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin2, { root: Symbol('test') }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin2, { root: {} }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin2, { root: f }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin2, { root: Unrelated2 }); }, ReferenceError);
        assert.doesNotThrow(() => { Mixin.define(Mixin2, { root: Related2 }); });

        class Mixin3 {}

        assert.throws(() => { Mixin.define(Mixin3, { invoke: false }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin3, { invoke: true }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin3, { invoke: 7 }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin3, { invoke: 3.14 }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin3, { invoke: 5n }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin3, { invoke: 'test' }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin3, { invoke: Symbol('test') }); }, TypeError);
        assert.throws(() => { Mixin.define(Mixin3, { invoke: {} }); }, TypeError);
    });
    test('.extends()', function () {
        const f = function () {};
        f.prototype = null;

        class Mixin1 {}

        assert.doesNotThrow(() => { Mixin.define(Mixin1); });

        assert.throws(() => { Mixin(Mixin1).extends(); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends(undefined); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends(null); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends(false); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends(true); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends(7); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends(3.14); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends(5n); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends('test'); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends(Symbol('test')); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends({}); }, TypeError);
        assert.throws(() => { Mixin(Mixin1).extends(f); }, TypeError);
    });
    test('.extends(): live adds only one interceptor', function () {
        class Mixin1 {}

        class Mixin2 {}

        class Mixin3 {}

        class Mixin4 {}

        class Class1 {}

        class Class2 extends Class1 {}

        class Class3 extends Class2 {}

        const classPrototype = Object.getPrototypeOf(Class2);
        const instancePrototype = Object.getPrototypeOf(Class2.prototype);
        Mixin.define(Mixin1);
        Mixin.define(Mixin2);
        Mixin.define(Mixin3);
        Mixin.define(Mixin4);
        assert.doesNotThrow(() => { Mixin(Mixin1).extends(Class2, { live: true }); });
        assert.doesNotThrow(() => { Mixin(Mixin2).extends(Class2, { live: true }); });
        assert.notStrictEqual(Object.getPrototypeOf(Class2), classPrototype);
        assert.notStrictEqual(Object.getPrototypeOf(Class2.prototype), instancePrototype);
        assert.strictEqual(Object.getPrototypeOf(Object.getPrototypeOf(Class2)), classPrototype);
        assert.strictEqual(Object.getPrototypeOf(Object.getPrototypeOf(Class2.prototype)), instancePrototype);
        const mixinProxy = Object.getPrototypeOf(Class2);
        assert.doesNotThrow(() => { Mixin(Mixin3).extends(mixinProxy, { live: true }); });
        assert.doesNotThrow(() => { Mixin(Mixin4).extends(Class3, { live: true }); });
        assert.strictEqual(mixinProxy, Object.getPrototypeOf(Class2));
        // eslint-disable-next-line no-unused-expressions
        assert.doesNotThrow(() => { mixinProxy instanceof Mixin; });
        let instance;
        assert.doesNotThrow(() => { instance = new Class2(); });
        assert.instanceOf(instance, Class1);
        assert.instanceOf(instance, Class2);
        assert.instanceOf(instance, Mixin1);
        assert.instanceOf(instance, Mixin2);
        assert.instanceOf(instance, Mixin3);
        assert.instanceOf(Object.getPrototypeOf(Class3.prototype), Mixin3);
        assert.instanceOf(Object.getPrototypeOf(Class3.prototype), Mixin(Mixin3));
    });
    test('extends(): live can be partially set on a class or a prototype', function () {
        class Mixin1 {}

        class Mixin2 {}

        class Class1 {}

        class Class2 extends Class1 {}

        class Class3 {}

        class Class4 extends Class3 {}

        const c2ClassPrototype = Object.getPrototypeOf(Class2);
        const c2InstancePrototype = Object.getPrototypeOf(Class2.prototype);
        const c4ClassPrototype = Object.getPrototypeOf(Class4);
        const c4InstancePrototype = Object.getPrototypeOf(Class4.prototype);
        Mixin.define(Mixin1);
        Mixin.define(Mixin2);
        assert.doesNotThrow(() => { Mixin(Mixin1).extends(Class2, { live: { static: true } }); });
        assert.doesNotThrow(() => { Mixin(Mixin2).extends(Class4, { live: { prototype: true } }); });
        assert.notStrictEqual(Object.getPrototypeOf(Class2), c2ClassPrototype);
        assert.strictEqual(Object.getPrototypeOf(Object.getPrototypeOf(Class2)), c2ClassPrototype);
        assert.strictEqual(Object.getPrototypeOf(Class2.prototype), c2InstancePrototype);
        assert.strictEqual(Object.getPrototypeOf(Class4), c4ClassPrototype);
        assert.notStrictEqual(Object.getPrototypeOf(Class4.prototype), c4InstancePrototype);
        assert.strictEqual(Object.getPrototypeOf(Object.getPrototypeOf(Class4.prototype)), c4InstancePrototype);
    });
    test('.toString()', function () {
        class Mixin1 {}

        class Mixin2 {}

        const Mixin3Factory = function () {
            return function () {};
        };

        const Mixin3 = Mixin3Factory();

        Mixin.define(Mixin1);
        Mixin.define(Mixin2);
        Mixin.define(Mixin3);

        assert.strictEqual('' + Mixin(Mixin1), 'mixin Mixin1');
        assert.strictEqual('' + Mixin(Mixin2), 'mixin Mixin2');
        assert.strictEqual('' + Mixin(Mixin3), 'mixin');
    });
    suite('constructor', function () {
        suite('copy', function () {
            test('new <Class>()', function () {
                const args = [{}, {}, {}];
                const factory = generator({
                    tree: {
                        Class1: {},
                        Mixin1: {}
                    },
                    mixins: {
                        Mixin1: 'Mixin1'
                    },
                    extends: {
                        Class1: ['Mixin1']
                    }
                });
                const liveOptions = {
                    extends: {
                        Mixin1: {
                            Class1: { live: true }
                        }
                    }
                };
                for (const live of [false, true]) {
                    const classes = factory(live ? liveOptions : {});
                    assert.instanceOf(classes.Mixin1, Mixin);
                    assert.notInstanceOf(classes.Class1, Mixin);
                    let instance;
                    assert.doesNotThrow(() => { instance = new classes.Class1(...args); });
                    assert.instanceOf(instance, classes.Class1);
                    assert.instanceOf(instance, classes.Mixin1);
                    assert.instanceOf(instance, Mixin(classes.Mixin1));
                    compare(instance.invocation, {
                        arguments: args,
                        className: 'Class1',
                        methodName: 'constructor',
                        mixins: {
                            Mixin1: {
                                arguments: args,
                                className: 'Mixin1',
                                methodName: 'constructor',
                                mixins: {},
                                newTarget: classes.Class1,
                                static: false,
                                super: null
                            }
                        },
                        newTarget: classes.Class1,
                        static: false,
                        super: null,
                        this: instance
                    });
                }
            });
            test('new <Mixin>()', function () {
                const args = [{}, {}, {}];
                const factory = generator({
                    tree: {
                        Class1: {},
                        Mixin1: {}
                    },
                    mixins: {
                        Mixin1: 'Mixin1'
                    },
                    extends: {
                        Class1: ['Mixin1']
                    }
                });
                const liveOptions = {
                    Mixin1: {
                        Class1: { live: true }
                    }
                };
                for (const live of [false, true]) {
                    const classes = factory(live ? liveOptions : {});
                    assert.instanceOf(classes.Mixin1, Mixin);
                    assert.notInstanceOf(classes.Class1, Mixin);
                    let instance;
                    assert.doesNotThrow(() => { instance = new classes.Mixin1(...args); });
                    assert.notInstanceOf(instance, classes.Class1);
                    assert.instanceOf(instance, classes.Mixin1);
                    compare(instance.invocation, {
                        arguments: args,
                        className: 'Mixin1',
                        methodName: 'constructor',
                        mixins: {},
                        newTarget: classes.Mixin1,
                        static: false,
                        super: null,
                        this: instance
                    });
                }
            });
            test('new <Class>(): criss-cross', function () {
                const args = [{}, {}, {}];
                const factory = generator({
                    tree: {
                        Class1: {
                            Class2: {}
                        },
                        Mixin1: {
                            Mixin2: {}
                        }
                    },
                    mixins: {
                        Mixin1: 'Mixin1',
                        Mixin2: 'Mixin1'
                    },
                    extends: {
                        Class1: ['Mixin2'],
                        Class2: ['Mixin1']
                    }
                });
                const liveOptions = {
                    Mixin1: {
                        Class1: { live: true }
                    }
                };
                for (const live of [false, true]) {
                    const classes = factory(live ? liveOptions : {});
                    assert.instanceOf(classes.Mixin1, Mixin);
                    assert.instanceOf(classes.Mixin2, Mixin);
                    assert.notInstanceOf(classes.Class1, Mixin);
                    assert.notInstanceOf(classes.Class2, Mixin);
                    {
                        let instance;
                        assert.doesNotThrow(() => { instance = new classes.Class1(...args); });
                        assert.instanceOf(instance, classes.Class1);
                        assert.notInstanceOf(instance, classes.Class2);
                        assert.instanceOf(instance, classes.Mixin2);
                        assert.instanceOf(instance, classes.Mixin1);
                        compare(instance.invocation, {
                            arguments: args,
                            className: 'Class1',
                            methodName: 'constructor',
                            mixins: {
                                Mixin2: {
                                    arguments: args,
                                    className: 'Mixin2',
                                    methodName: 'constructor',
                                    mixins: {},
                                    newTarget: classes.Class1,
                                    static: false,
                                    super: {
                                        arguments: args,
                                        className: 'Mixin1',
                                        methodName: 'constructor',
                                        mixins: {},
                                        newTarget: classes.Class1,
                                        static: false
                                    }
                                }
                            },
                            newTarget: classes.Class1,
                            static: false,
                            super: null,
                            this: instance
                        });
                    }
                    {
                        let instance;
                        assert.doesNotThrow(() => { instance = new classes.Class2(...args); });
                        assert.instanceOf(instance, classes.Class2);
                        assert.instanceOf(instance, classes.Class1);
                        assert.instanceOf(instance, classes.Mixin2);
                        assert.instanceOf(instance, classes.Mixin1);
                        compare(instance.invocation, {
                            arguments: args,
                            className: 'Class2',
                            methodName: 'constructor',
                            mixins: {
                                Mixin1: {
                                    arguments: args,
                                    className: 'Mixin1',
                                    methodName: 'constructor',
                                    mixins: {},
                                    newTarget: classes.Class2,
                                    static: false
                                }
                            },
                            newTarget: classes.Class2,
                            static: false,
                            super: {
                                arguments: args,
                                className: 'Class1',
                                methodName: 'constructor',
                                mixins: {
                                    Mixin2: {
                                        arguments: args,
                                        className: 'Mixin2',
                                        methodName: 'constructor',
                                        mixins: {},
                                        newTarget: classes.Class1,
                                        static: false,
                                        super: {
                                            arguments: args,
                                            className: 'Mixin1',
                                            methodName: 'constructor',
                                            mixins: {},
                                            newTarget: classes.Class1,
                                            static: false
                                        }
                                    }
                                },
                                newTarget: classes.Class2,
                                static: false,
                                super: null,
                                this: instance
                            },
                            this: instance
                        });
                    }
                }
            });
            test('invalid this', function () {
                for (const live of [false, true]) {
                    const wrongThis = {};

                    class Class1 {
                        constructor() {
                            Mixin(Mixin1).apply(wrongThis, []);
                        }
                    }

                    class Mixin1 {
                    }

                    Mixin.define(Mixin1);
                    Mixin(Mixin1).extends(Class1, { live });
                    // eslint-disable-next-line no-new
                    assert.throws(() => { new Class1(); }, TypeError);
                }
            });
            test('property transfer', function () {
                for (const live of [false, true]) {
                    const testSymbol = Symbol('test');
                    const testObjects = [{}, {}];

                    class Class1 {
                        constructor() {
                            Mixin(Mixin1).apply(this, []);
                        }
                    }

                    class Mixin1 {
                        constructor() {
                            this.test = testObjects[0];
                            this[testSymbol] = testObjects[1];
                        }
                    }

                    Mixin.define(Mixin1);
                    Mixin(Mixin1).extends(Class1, { live });
                    let instance;
                    assert.doesNotThrow(() => { instance = new Class1(); });
                    assert(Object.prototype.hasOwnProperty.call(instance, 'test'), `has property: test`);
                    assert.strictEqual(instance.test, testObjects[0]);
                    assert(Object.prototype.hasOwnProperty.call(instance, testSymbol), `has property: Symbol(test)`);
                    assert.strictEqual(instance[testSymbol], testObjects[1]);
                }
            });
        });
        suite('deny', function () {
            test('valid this', function () {
                for (const live of [false, true]) {
                    let called = 0;

                    class Class1 {
                        constructor() {
                            Mixin(Mixin1).apply(this, []);
                        }
                    }

                    class Mixin1 {
                        constructor() {
                            ++called;
                        }
                    }

                    Mixin.define(Mixin1, { invoke: 'deny' });
                    Mixin(Mixin1).extends(Class1, { live });
                    // eslint-disable-next-line no-new
                    assert.throws(() => { new Class1(); }, TypeError);
                    assert.strictEqual(called, 0);
                }
            });
            test('invalid this', function () {
                const wrongThis = {};
                for (const live of [false, true]) {
                    let called = 0;

                    class Class1 {
                        constructor() {
                            Mixin(Mixin1).apply(wrongThis, []);
                        }
                    }

                    class Mixin1 {
                        constructor() {
                            ++called;
                        }
                    }

                    Mixin.define(Mixin1, { invoke: 'deny' });
                    Mixin(Mixin1).extends(Class1, { live });
                    // eslint-disable-next-line no-new
                    assert.throws(() => { new Class1(); }, TypeError);
                    assert.strictEqual(called, 0);
                }
            });
        });
        suite('call', function () {
            test('new <Class>()', function () {
                const args = [{}, {}, {}];
                const factory = generator({
                    tree: {
                        Class1: {},
                        Mixin1: {}
                    },
                    mixins: {
                        Mixin1: 'Mixin1'
                    },
                    extends: {
                        Class1: ['Mixin1']
                    },
                    class: {
                        Mixin1: { es5: true }
                    }
                });
                for (const live of [false, true]) {
                    const classes = factory({
                        extends: {
                            Mixin1: {
                                Class1: { live }
                            }
                        },
                        define: {
                            Mixin1: { invoke: 'call' }
                        }
                    });
                    assert.instanceOf(classes.Mixin1, Mixin);
                    assert.notInstanceOf(classes.Class1, Mixin);
                    let instance;
                    assert.doesNotThrow(() => { instance = new classes.Class1(...args); });
                    assert.instanceOf(instance, classes.Class1);
                    assert.instanceOf(instance, classes.Mixin1);
                    assert.instanceOf(instance, Mixin(classes.Mixin1));
                    compare(instance.invocation, {
                        arguments: args,
                        className: 'Class1',
                        methodName: 'constructor',
                        mixins: {
                            Mixin1: {
                                arguments: args,
                                className: 'Mixin1',
                                methodName: 'constructor',
                                mixins: {},
                                static: false,
                                super: null
                            }
                        },
                        newTarget: classes.Class1,
                        static: false,
                        super: null,
                        this: instance
                    });
                }
            });
            test('invalid this', function () {
                const wrongThis = {};
                for (const live of [false, true]) {
                    let called = 0;

                    class Class1 {
                        constructor() {
                            Mixin(Mixin1).apply(wrongThis, []);
                        }
                    }

                    class Mixin1 {
                        constructor() {
                            ++called;
                        }
                    }

                    Mixin.define(Mixin1, { invoke: 'call' });
                    Mixin(Mixin1).extends(Class1, { live });
                    // eslint-disable-next-line no-new
                    assert.throws(() => { new Class1(); }, TypeError);
                    assert.strictEqual(called, 0);
                }
            });
        });
        suite('method', function () {
            test('new <Class>()', function () {
                const args = [{}, {}, {}];
                const factory = generator({
                    tree: {
                        Class1: {},
                        Mixin1: {}
                    },
                    mixins: {
                        Mixin1: 'Mixin1'
                    },
                    extends: {
                        Class1: ['Mixin1']
                    },
                    methods: {
                        Mixin1: {
                            prototype: {
                                initialize: {}
                            }
                        }
                    },
                    class: {
                        Mixin1: { returnsFromConstructor: true }
                    }
                });
                for (const live of [false, true]) {
                    const classes = factory({
                        extends: {
                            Mixin1: {
                                Class1: { live }
                            }
                        },
                        define: {
                            Mixin1: {
                                invoke: 'method',
                                methodName: 'initialize'
                            }
                        }
                    });
                    assert.instanceOf(classes.Mixin1, Mixin);
                    assert.notInstanceOf(classes.Class1, Mixin);
                    let instance;
                    assert.doesNotThrow(() => { instance = new classes.Class1(...args); });
                    assert.instanceOf(instance, classes.Class1);
                    assert.instanceOf(instance, classes.Mixin1);
                    assert.instanceOf(instance, Mixin(classes.Mixin1));
                    compare(instance.invocation, {
                        arguments: args,
                        className: 'Class1',
                        methodName: 'constructor',
                        mixins: {
                            Mixin1: {
                                arguments: args,
                                className: 'Mixin1',
                                methodName: 'initialize',
                                mixins: {},
                                static: false,
                                super: null,
                                this: instance
                            }
                        },
                        newTarget: classes.Class1,
                        static: false,
                        super: null,
                        this: instance
                    });
                }
            });
            test('missing method option', function () {
                class Mixin1 {
                }

                assert.throws(() => { Mixin.define(Mixin1, { invoke: 'method' }); }, TypeError);
            });
            test('missing method', function () {
                const args = [{}, {}, {}];
                const factory = generator({
                    tree: {
                        Class1: {},
                        Mixin1: {}
                    },
                    mixins: {
                        Mixin1: 'Mixin1'
                    },
                    extends: {
                        Class1: ['Mixin1']
                    },
                    class: {
                        Mixin1: { returnsFromConstructor: true }
                    }
                });
                for (const live of [false, true]) {
                    const classes = factory({
                        extends: {
                            Mixin1: {
                                Class1: { live }
                            }
                        },
                        define: {
                            Mixin1: {
                                invoke: 'method',
                                methodName: 'initialize'
                            }
                        }
                    });
                    assert.instanceOf(classes.Mixin1, Mixin);
                    assert.notInstanceOf(classes.Class1, Mixin);
                    // eslint-disable-next-line no-new
                    assert.throws(() => { new classes.Class1(...args); }, ReferenceError);
                }
            });

            test('invalid this', function () {
                const wrongThis = {};
                for (const live of [false, true]) {
                    let called = 0;

                    class Class1 {
                        constructor() {
                            Mixin(Mixin1).apply(wrongThis, []);
                        }
                    }

                    class Mixin1 {
                        constructor() {
                            ++called;
                        }

                        test() {
                            ++called;
                        }
                    }

                    Mixin.define(Mixin1, {
                        invoke: 'method',
                        methodName: 'test'
                    });
                    Mixin(Mixin1).extends(Class1, { live });
                    // eslint-disable-next-line no-new
                    assert.throws(() => { new Class1(); }, TypeError);
                    assert.strictEqual(called, 0);
                }
            });
        });
        suite('static', function () {
            test('new <Class>()', function () {
                const args = [{}, {}, {}];
                const factory = generator({
                    tree: {
                        Class1: {},
                        Mixin1: {}
                    },
                    mixins: {
                        Mixin1: 'Mixin1'
                    },
                    extends: {
                        Class1: ['Mixin1']
                    },
                    methods: {
                        Mixin1: {
                            static: {
                                initialize: {}
                            }
                        }
                    },
                    class: {
                        Mixin1: { returnsFromConstructor: true }
                    }
                });
                for (const live of [false, true]) {
                    const classes = factory({
                        extends: {
                            Mixin1: {
                                Class1: { live }
                            }
                        },
                        define: {
                            Mixin1: {
                                invoke: 'static',
                                methodName: 'initialize'
                            }
                        }
                    });
                    assert.instanceOf(classes.Mixin1, Mixin);
                    assert.notInstanceOf(classes.Class1, Mixin);
                    let instance;
                    assert.doesNotThrow(() => { instance = new classes.Class1(...args); });
                    assert.instanceOf(instance, classes.Class1);
                    assert.instanceOf(instance, classes.Mixin1);
                    assert.instanceOf(instance, Mixin(classes.Mixin1));
                    compare(instance.invocation, {
                        arguments: args,
                        className: 'Class1',
                        methodName: 'constructor',
                        mixins: {
                            Mixin1: {
                                arguments: args,
                                className: 'Mixin1',
                                methodName: 'initialize',
                                mixins: {},
                                static: true,
                                super: null,
                                this: instance
                            }
                        },
                        newTarget: classes.Class1,
                        static: false,
                        super: null,
                        this: instance
                    });
                }
            });
            test('missing method option', function () {
                class Mixin1 {
                }

                assert.throws(() => { Mixin.define(Mixin1, { invoke: 'static' }); }, TypeError);
            });
            test('missing method', function () {
                const args = [{}, {}, {}];
                const factory = generator({
                    tree: {
                        Class1: {},
                        Mixin1: {}
                    },
                    mixins: {
                        Mixin1: 'Mixin1'
                    },
                    extends: {
                        Class1: ['Mixin1']
                    },
                    class: {
                        Mixin1: { returnsFromConstructor: true }
                    }
                });
                for (const live of [false, true]) {
                    const classes = factory({
                        extends: {
                            Mixin1: {
                                Class1: { live }
                            }
                        },
                        define: {
                            Mixin1: {
                                invoke: 'static',
                                methodName: 'initialize'
                            }
                        }
                    });
                    assert.instanceOf(classes.Mixin1, Mixin);
                    assert.notInstanceOf(classes.Class1, Mixin);
                    // eslint-disable-next-line no-new
                    assert.throws(() => { new classes.Class1(...args); }, ReferenceError);
                }
            });

            test('invalid this', function () {
                const wrongThis = {};
                for (const live of [false, true]) {
                    let called = 0;

                    class Class1 {
                        constructor() {
                            Mixin(Mixin1).apply(wrongThis, []);
                        }
                    }

                    class Mixin1 {
                        constructor() {
                            ++called;
                        }

                        static test() {
                            ++called;
                        }
                    }

                    Mixin.define(Mixin1, {
                        invoke: 'static',
                        methodName: 'test'
                    });
                    Mixin(Mixin1).extends(Class1, { live });
                    // eslint-disable-next-line no-new
                    assert.throws(() => { new Class1(); }, TypeError);
                    assert.strictEqual(called, 0);
                }
            });
        });
    });
    suite('inheritance', function () {
        test('can inherit static method', function () {
            const args = [{}, {}, {}];
            const factory = generator({
                tree: {
                    Class1: {},
                    Mixin1: {}
                },
                mixins: {
                    Mixin1: 'Mixin1'
                },
                extends: {
                    Class1: ['Mixin1']
                },
                methods: {
                    Mixin1: {
                        static: {
                            test: {}
                        }
                    }
                },
                class: {
                    Mixin1: { returnsFromConstructor: true }
                }
            });
            for (const live of [false, true]) {
                const classes = factory({
                    extends: {
                        Mixin1: {
                            Class1: { live }
                        }
                    }
                });
                let invocation;
                assert.doesNotThrow(() => { invocation = classes.Class1.test(...args); });
                compare(invocation, {
                    arguments: args,
                    className: 'Mixin1',
                    methodName: 'test',
                    mixins: {},
                    newTarget: undefined,
                    static: true,
                    super: null,
                    this: classes.Class1
                });
            }
        });
        test('can inherit prototype method', function () {
            const args = [{}, {}, {}];
            const factory = generator({
                tree: {
                    Class1: {},
                    Mixin1: {}
                },
                mixins: {
                    Mixin1: 'Mixin1'
                },
                extends: {
                    Class1: ['Mixin1']
                },
                methods: {
                    Mixin1: {
                        prototype: {
                            test: {}
                        }
                    }
                },
                class: {
                    Mixin1: { returnsFromConstructor: true }
                }
            });
            for (const live of [false, true]) {
                const classes = factory({
                    extends: {
                        Mixin1: {
                            Class1: { live }
                        }
                    }
                });
                const instance = new classes.Class1();
                let invocation;
                assert.doesNotThrow(() => { invocation = instance.test(...args); });
                compare(invocation, {
                    arguments: args,
                    className: 'Mixin1',
                    methodName: 'test',
                    mixins: {},
                    newTarget: undefined,
                    static: false,
                    super: null,
                    this: instance
                });
            }
        });
        test('class can be extended by multiple mixins', function () {
            const args = [{}, {}, {}];
            const factory = generator({
                tree: {
                    Class1: {},
                    Mixin1: {},
                    Mixin2: {}
                },
                mixins: {
                    Mixin1: 'Mixin1',
                    Mixin2: 'Mixin2'
                },
                extends: {
                    Class1: ['Mixin1', 'Mixin2']
                },
                methods: {
                    Mixin1: {
                        static: {
                            static1: {}
                        },
                        prototype: {
                            method1: {}
                        }
                    },
                    Mixin2: {
                        static: {
                            static2: {}
                        },
                        prototype: {
                            method2: {}
                        }
                    }
                }
            });
            for (const live of [false, true]) {
                const classes = factory({
                    extends: {
                        Mixin1: {
                            Class1: { live }
                        },
                        Mixin2: {
                            Class1: { live }
                        }
                    }
                });
                const instance = new classes.Class1();
                let invocation;
                assert.doesNotThrow(() => { invocation = classes.Class1.static1(...args); });
                compare(invocation, {
                    arguments: args,
                    className: 'Mixin1',
                    methodName: 'static1',
                    mixins: {},
                    newTarget: undefined,
                    static: true,
                    super: null,
                    this: classes.Class1
                });
                assert.doesNotThrow(() => { invocation = classes.Class1.static2(...args); });
                compare(invocation, {
                    arguments: args,
                    className: 'Mixin2',
                    methodName: 'static2',
                    mixins: {},
                    newTarget: undefined,
                    static: true,
                    super: null,
                    this: classes.Class1
                });
                assert.doesNotThrow(() => { invocation = instance.method1(...args); });
                compare(invocation, {
                    arguments: args,
                    className: 'Mixin1',
                    methodName: 'method1',
                    mixins: {},
                    newTarget: undefined,
                    static: false,
                    super: null,
                    this: instance
                });
                assert.doesNotThrow(() => { invocation = instance.method2(...args); });
                compare(invocation, {
                    arguments: args,
                    className: 'Mixin2',
                    methodName: 'method2',
                    mixins: {},
                    newTarget: undefined,
                    static: false,
                    super: null,
                    this: instance
                });
            }
        });
        test('mixin prototype chain is observed until root', function () {
            const args = [{}, {}, {}];
            const factory = generator({
                tree: {
                    Class1: {},
                    Mixin1: {
                        Mixin2: {
                            Mixin3: {
                                Mixin4: {
                                    Mixin5: {}
                                }
                            }
                        }
                    }
                },
                mixins: {
                    Mixin5: 'Mixin3',
                    Mixin4: 'Mixin3'
                },
                extends: {
                    Class1: ['Mixin5']
                },
                methods: {
                    Mixin1: {
                        static: {
                            static1: {}
                        },
                        prototype: {
                            method1: {}
                        }
                    },
                    Mixin2: {
                        static: {
                            static2: {}
                        },
                        prototype: {
                            method2: {}
                        }
                    },
                    Mixin3: {
                        static: {
                            static3: {}
                        },
                        prototype: {
                            method3: {}
                        }
                    },
                    Mixin4: {
                        static: {
                            static4: {}
                        },
                        prototype: {
                            method4: {}
                        }
                    },
                    Mixin5: {
                        static: {
                            static5: {}
                        },
                        prototype: {
                            method5: {}
                        }
                    }
                }
            });
            for (const live of [false, true]) {
                const classes = factory({
                    extends: {
                        Mixin5: {
                            Class1: { live }
                        }
                    }
                });
                {
                    let invocation;
                    assert('static5' in classes.Class1, `'static5' in Class1`);
                    assert.doesNotThrow(() => { invocation = classes.Class1.static5(...args); });
                    compare(invocation, {
                        arguments: args,
                        className: 'Mixin5',
                        methodName: 'static5',
                        mixins: {},
                        newTarget: undefined,
                        static: true,
                        super: null,
                        this: classes.Class1
                    });
                }
                {
                    let invocation;
                    assert('static4' in classes.Class1, `'static4' in Class1`);
                    assert.doesNotThrow(() => { invocation = classes.Class1.static4(...args); });
                    compare(invocation, {
                        arguments: args,
                        className: 'Mixin4',
                        methodName: 'static4',
                        mixins: {},
                        newTarget: undefined,
                        static: true,
                        super: null,
                        this: classes.Class1
                    });
                }
                {
                    let invocation;
                    assert('static3' in classes.Class1, `'static3' in Class1`);
                    assert.doesNotThrow(() => { invocation = classes.Class1.static3(...args); });
                    compare(invocation, {
                        arguments: args,
                        className: 'Mixin3',
                        methodName: 'static3',
                        mixins: {},
                        newTarget: undefined,
                        static: true,
                        super: null,
                        this: classes.Class1
                    });
                }
                assert(!('static2' in classes.Class1), `'static2' in Class1`);
                assert(typeof classes.Class1.static2 !== 'function', `typeof classes.Class1.static2 !== 'function'`);
                assert(!('static1' in classes.Class1), `'static1' in Class1`);
                assert(typeof classes.Class1.static1 !== 'function', `typeof classes.Class1.static1 !== 'function'`);
                const instance = new classes.Class1();
                {
                    let invocation;
                    assert('method5' in instance, `'method5' in instance`);
                    assert.doesNotThrow(() => { invocation = instance.method5(...args); });
                    compare(invocation, {
                        arguments: args,
                        className: 'Mixin5',
                        methodName: 'method5',
                        mixins: {},
                        newTarget: undefined,
                        static: false,
                        super: null,
                        this: instance
                    });
                }
                {
                    let invocation;
                    assert('method4' in instance, `'method4' in instance`);
                    assert.doesNotThrow(() => { invocation = instance.method4(...args); });
                    compare(invocation, {
                        arguments: args,
                        className: 'Mixin4',
                        methodName: 'method4',
                        mixins: {},
                        newTarget: undefined,
                        static: false,
                        super: null,
                        this: instance
                    });
                }
                {
                    let invocation;
                    assert('method3' in instance, `'method3' in instance`);
                    assert.doesNotThrow(() => { invocation = instance.method3(...args); });
                    compare(invocation, {
                        arguments: args,
                        className: 'Mixin3',
                        methodName: 'method3',
                        mixins: {},
                        newTarget: undefined,
                        static: false,
                        super: null,
                        this: instance
                    });
                }
                assert(!('method2' in instance), `'method2' in instance`);
                assert(typeof instance.method2 !== 'function', `typeof instance.method2 !== 'function'`);
                assert(!('method1' in instance), `'method1' in instance`);
                assert(typeof instance.method1 !== 'function', `typeof instance.method1 !== 'function'`);

                assert.instanceOf(instance, classes.Mixin5);
                assert.instanceOf(instance, classes.Mixin4);
                assert.instanceOf(instance, classes.Mixin3);
                assert.notInstanceOf(instance, classes.Mixin2);
                assert.notInstanceOf(instance, classes.Mixin1);
            }
        });
        test('ECMAScript prototype chain takes precedence over mixins', function () {
            for (const live of [false, true]) {
                let validCall = 0;
                let invalidCall = 0;

                class Class1 {
                    static static1() {
                        ++validCall;
                    }

                    method1() {
                        ++validCall;
                    }
                }

                class Class2 extends Class1 {}

                class Mixin1 {
                    static static1() {
                        ++invalidCall;
                    }

                    method1() {
                        ++invalidCall;
                    }
                }

                Mixin.define(Mixin1).extends(Class2, { live });

                assert.doesNotThrow(() => { Class2.static1(); });
                assert.strictEqual(validCall, 1, `Class1::static1 was called`);
                assert.strictEqual(invalidCall, 0, `Mixin1::static1 was not called`);

                invalidCall = validCall = 0;

                const instance = new Class2();
                assert.instanceOf(instance, Mixin1);
                assert.doesNotThrow(() => { instance.method1(); });
                assert.strictEqual(validCall, 1, `Class1::static1 was called`);
                assert.strictEqual(invalidCall, 0, `Mixin1::static1 was not called`);
            }
        });
        test('Mixed live and non-live extensions', function () {
            const calls = [];

            class Class1 {}

            class Mixin1 {
                static static1() {
                    calls.push('Mixin1::static1');
                }

                method1() {
                    calls.push('Mixin1::method1');
                }
            }

            class Mixin2 {
                static static2() {
                    calls.push('Mixin2::static2');
                }

                method2() {
                    calls.push('Mixin2::method2');
                }
            }

            Mixin.define(Mixin1).extends(Class1, { live: false });
            Mixin.define(Mixin2).extends(Class1, { live: true });

            assert('static1' in Class1);
            assert(typeof Class1.static1 === 'function');
            assert('static2' in Class1);
            assert(typeof Class1.static2 === 'function');
            assert.doesNotThrow(() => { Class1.static1(); });
            assert.doesNotThrow(() => { Class1.static2(); });
            assert.strictEqual(calls[0], 'Mixin1::static1');
            assert.strictEqual(calls[1], 'Mixin2::static2');

            const instance = new Class1();
            calls.length = 0;

            assert('method1' in instance);
            assert(typeof instance.method1 === 'function');
            assert('method2' in instance);
            assert(typeof instance.method2 === 'function');
            assert.doesNotThrow(() => { instance.method1(); });
            assert.doesNotThrow(() => { instance.method2(); });
            assert.strictEqual(calls[0], 'Mixin1::method1');
            assert.strictEqual(calls[1], 'Mixin2::method2');
        });
    });
});
