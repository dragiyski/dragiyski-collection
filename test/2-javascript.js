/* eslint-disable camelcase */
import { assert } from 'chai';
import { suite, test } from 'mocha';
import JavaScript from '../src/javascript.js';

/* Call sites may or may not have more properties */
/* The actual flatten exports more properties, but validates whether there are functions required for thos properties */
const interfaceCallSite = Object.assign(Object.create(null), {
    columnNumber: 'getColumnNumber',
    filename: 'getFileName',
    lineNumber: 'getLineNumber',
    string: 'toString'
});

suite('JavaScript', function () {
    test('deserializeUnicodeSequences()', function () {
        assert.strictEqual(JavaScript.deserializeUnicodeSequences('test'), 'test');
        assert.strictEqual(JavaScript.deserializeUnicodeSequences(''), '');
        assert.strictEqual(JavaScript.deserializeUnicodeSequences('true'), 'true');
        assert.strictEqual(JavaScript.deserializeUnicodeSequences('\\u0074\\u0065\\u0073\\u0074'), 'test');
        assert.strictEqual(JavaScript.deserializeUnicodeSequences('\\u{10083}\\u{100CD}'), '𐂃𐃍');
    });
    test('isIdentifierOrKeyword()', function () {
        assert.strictEqual(JavaScript.isIdentifierOrKeyword(''), false);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('42'), false);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('[Symbol.iterator]'), false);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('Array Iterator'), false);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('test'), true);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('true'), true);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('class'), true);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('\\u0074\\u0065\\u0073\\u0074'), true);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('\\u{10083}\\u{100CD}'), true);
    });
    test('isIdentifier()', function () {
        assert.strictEqual(JavaScript.isIdentifier(''), false);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('42'), false);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('[Symbol.iterator]'), false);
        assert.strictEqual(JavaScript.isIdentifierOrKeyword('Array Iterator'), false);
        assert.strictEqual(JavaScript.isIdentifier('test'), true);
        assert.strictEqual(JavaScript.isIdentifier('true'), false);
        assert.strictEqual(JavaScript.isIdentifier('class'), false);
        assert.strictEqual(JavaScript.isIdentifier('\\u0074\\u0065\\u0073\\u0074'), true);
        assert.strictEqual(JavaScript.isIdentifier('\\u{10083}\\u{100CD}'), true);
    });
    test('canStoreProperties()', function () {
        assert.strictEqual(JavaScript.canStoreProperties(undefined), false);
        assert.strictEqual(JavaScript.canStoreProperties(null), false);
        assert.strictEqual(JavaScript.canStoreProperties(false), false);
        assert.strictEqual(JavaScript.canStoreProperties(true), false);
        assert.strictEqual(JavaScript.canStoreProperties(0), false);
        assert.strictEqual(JavaScript.canStoreProperties(5), false);
        assert.strictEqual(JavaScript.canStoreProperties(3.14), false);
        assert.strictEqual(JavaScript.canStoreProperties(10n), false);
        assert.strictEqual(JavaScript.canStoreProperties('test'), false);
        assert.strictEqual(JavaScript.canStoreProperties(Symbol('test')), false);
        assert.strictEqual(JavaScript.canStoreProperties({}), true);
        assert.strictEqual(JavaScript.canStoreProperties(function () {}), true);
    });
    test('getType()', function () {
        assert.strictEqual(JavaScript.getType(undefined), 'undefined');
        assert.strictEqual(JavaScript.getType(null), 'null');
        assert.strictEqual(JavaScript.getType(false), 'boolean');
        assert.strictEqual(JavaScript.getType(true), 'boolean');
        assert.strictEqual(JavaScript.getType(0), 'number');
        assert.strictEqual(JavaScript.getType(5), 'number');
        assert.strictEqual(JavaScript.getType(3.14), 'number');
        assert.strictEqual(JavaScript.getType(10n), 'bigint');
        assert.strictEqual(JavaScript.getType('test'), 'string');
        assert.strictEqual(JavaScript.getType(Symbol('test')), 'symbol');
        assert.strictEqual(JavaScript.getType({}), '[object Object]');
        assert.strictEqual(JavaScript.getType(function () {}), 'function');
        assert.strictEqual(JavaScript.getType(function * () {}), 'function');
        assert.strictEqual(JavaScript.getType(function Test() {}), '[function Test]');
        {
            const Test = function Test() {};
            const test = new Test();
            assert.strictEqual(JavaScript.getType(Test), '[function Test]');
            assert.strictEqual(JavaScript.getType(test), '[object Object]');
        }
        {
            const Test = class Test {};
            const test = new Test();
            assert.strictEqual(JavaScript.getType(Test), '[function Test]');
            assert.strictEqual(JavaScript.getType(test), '[object Object]');
        }
        {
            const Bar = function () {};
            Object.defineProperty(Bar.prototype, Symbol.toStringTag, {
                configurable: true,
                value: 'Test'
            });
            const test = new Bar();
            assert.strictEqual(JavaScript.getType(Bar), '[function Bar]');
            assert.strictEqual(JavaScript.getType(test), '[object Test]');
        }
        {
            const Test = function Foo() {};
            Object.defineProperty(Test.prototype, Symbol.toStringTag, {
                configurable: true,
                value: 'Test'
            });
            const test = new Test();
            assert.strictEqual(JavaScript.getType(Test), '[function Foo]');
            assert.strictEqual(JavaScript.getType(test), '[object Test]');
        }
    });
    test('prototypeOf()', function () {
        assert.strictEqual(JavaScript.prototypeOf(), false);
        assert.strictEqual(JavaScript.prototypeOf({}), false);
        assert.strictEqual(JavaScript.prototypeOf({}, null), false);
        assert.strictEqual(JavaScript.prototypeOf({}, {}), false);
        assert.strictEqual(JavaScript.prototypeOf({}, Object.prototype), true);
        assert.strictEqual(JavaScript.prototypeOf(Object.create(null), Object.prototype), false);
        {
            const node = Object.create(null);
            const node_1 = Object.create(node);
            const node_2 = Object.create(node);
            const node_1_1 = Object.create(node_1);
            const node_1_2 = Object.create(node_1);
            const node_2_1 = Object.create(node_2);
            const node_2_2 = Object.create(node_2);
            assert.strictEqual(JavaScript.prototypeOf(node, node), false);
            assert.strictEqual(JavaScript.prototypeOf(node_1, node), true);
            assert.strictEqual(JavaScript.prototypeOf(node_2, node), true);
            assert.strictEqual(JavaScript.prototypeOf(node_1_1, node), true);
            assert.strictEqual(JavaScript.prototypeOf(node_1_1, node_1), true);
            assert.strictEqual(JavaScript.prototypeOf(node_1_2, node), true);
            assert.strictEqual(JavaScript.prototypeOf(node_1_2, node_1), true);
            assert.strictEqual(JavaScript.prototypeOf(node_2_1, node), true);
            assert.strictEqual(JavaScript.prototypeOf(node_2_2, node_2), true);
            assert.strictEqual(JavaScript.prototypeOf(node_1_2, node_2), false);
            assert.strictEqual(JavaScript.prototypeOf(node_1_2, node_2_2), false);
            assert.strictEqual(JavaScript.prototypeOf(node_2_1, node_1), false);
            assert.strictEqual(JavaScript.prototypeOf(node_2_1, node_1_2), false);
        }
    });
    test('instanceOf()', function () {
        assert.strictEqual(JavaScript.instanceOf(), false);
        assert.strictEqual(JavaScript.instanceOf(null), false);
        assert.strictEqual(JavaScript.instanceOf(null, null), false);
        assert.strictEqual(JavaScript.instanceOf({}, null), false);
        assert.strictEqual(JavaScript.instanceOf({}, function () {}), false);
        {
            const notClass = function () {};
            notClass.prototype = null;
            // eslint-disable-next-line new-cap
            const object = new notClass();
            assert.strictEqual(JavaScript.instanceOf(object, notClass), false);
        }
        {
            const Class = function () {};
            const object = new Class();
            assert.strictEqual(JavaScript.instanceOf(object, Class), true);
        }
        {
            const Class = function () {};
            const notInstance = {};
            Object.defineProperty(Class, Symbol.hasInstance, {
                configurable: true,
                value: function (instance) {
                    if (instance === notInstance) {
                        return true;
                    }
                    return Function.prototype[Symbol.hasInstance].apply(this, arguments);
                }
            });
            assert(notInstance instanceof Class, `notInstance instanceof Class`);
            assert.strictEqual(JavaScript.instanceOf(notInstance, Class), false);
        }
    });
    test('getStackTrace()', function () {
        {
            const currentTrace = JavaScript.getStackTrace();
            assert(Array.isArray(currentTrace), `currentTrace is Array`);
            assert.isAbove(currentTrace.length, 0);
            for (let i = 0; i < currentTrace.length; ++i) {
                const frame = currentTrace[i];
                for (const name in interfaceCallSite) {
                    assert(name in frame, `${name} should be in the stack trace frame`);
                }
            }
        }
        {
            const error = new Error();
            const errorTrace = JavaScript.getStackTrace(error);
            const currentTrace = JavaScript.getStackTrace();
            assert(Array.isArray(errorTrace), `errorTrace is Array`);
            assert(Array.isArray(currentTrace), `currentTrace is Array`);
            assert.isAbove(errorTrace.length, 0);
            for (let i = 0; i < errorTrace.length; ++i) {
                const frame = errorTrace[i];
                for (const name in interfaceCallSite) {
                    assert(name in frame, `${name} should be in the stack trace frame`);
                }
            }
            assert.strictEqual(errorTrace.length, currentTrace.length);
            for (let i = 0; i < errorTrace.length; ++i) {
                for (const name in interfaceCallSite) {
                    if (['position', 'lineNumber', 'columnNumber'].indexOf(name) >= 0) {
                        continue;
                    }
                    assert.strictEqual(errorTrace[name], currentTrace[name]);
                }
                assert.strictEqual(errorTrace[0].lineNumber, currentTrace[0].lineNumber - 2);
            }
            assert(typeof error.stack === 'string', `error.stack should be string`);
            const stack = error.stack.split('\n');
            stack.shift();
            for (const line of stack) {
                assert.match(line, /^\s+at\s+/);
            }
        }
        {
            const error = new Error();
            const stack = error.stack;
            assert.strictEqual(JavaScript.getStackTrace(error), null);
            assert.strictEqual(error.stack, stack);
        }
        {
            const error = new Error();
            error.stack = 5;
            assert.strictEqual(JavaScript.getStackTrace(error), null);
        }
        {
            const originalPrepareStackTrace = Object.getOwnPropertyDescriptor(Error, 'prepareStackTrace');
            const prepareStackTrace = (error, trace) => String(error) + trace.map(frame => `\n    at ${frame}`);
            const expectedDescriptor = {
                configurable: true,
                enumerable: false,
                writable: true,
                value: prepareStackTrace
            };
            if (originalPrepareStackTrace != null && !originalPrepareStackTrace.configurable) {
                return void this.skip();
            }
            Object.defineProperty(Error, 'prepareStackTrace', expectedDescriptor);
            JavaScript.getStackTrace();
            const afterDescriptor = Object.getOwnPropertyDescriptor(Error, 'prepareStackTrace');
            if (originalPrepareStackTrace != null) {
                Object.defineProperty(Error, 'prepareStackTrace', originalPrepareStackTrace);
            } else {
                delete Error.prepareStackTrace;
            }
            assert(afterDescriptor != null, `JavaScript.getStackTrace should restore the original Error.prepareStackTrace`);
            for (const key of Object.keys(expectedDescriptor)) {
                assert(Object.hasOwnProperty.call(afterDescriptor, key), `JavaScript.getStackTrace should restore the original Error.prepareStackTrace`);
                assert.strictEqual(afterDescriptor[key], expectedDescriptor[key], `JavaScript.getStackTrace should restore the original Error.prepareStackTrace`, expectedDescriptor.key);
            }
        }
    });
});
