import { assert } from 'chai';
import { suite, test } from 'mocha';
import OrderedSet from '../src/ordered-set.js';

suite('OrderedSet', () => {
    test('is class', function () {
        assert(typeof OrderedSet === 'function' && OrderedSet.prototype != null, `OrderedSet should be a class/constructor function`);
    });
    test('new', function () {
        let orderedSet = new OrderedSet();
        assert.doesNotThrow(() => {
            orderedSet = new OrderedSet();
        });
        assert.strictEqual(orderedSet.length, 0, `Newly initialized OrderedSet should be empty`);
        const symbol = Symbol('test');
        assert.doesNotThrow(() => {
            'use strict';
            orderedSet[symbol] = true;
            assert(symbol in orderedSet, `Index handler should not affect symbol properties`);
        });
        assert.equal(Object.prototype.toString.call(orderedSet), '[object OrderedSet]');
    });
    test('append()', function () {
        const items = [{}, {}, 3, 'test', {}];
        const list = new OrderedSet();
        for (const item of items) {
            list.append(item);
        }
        list.append(items[1]);
        assert.strictEqual(list.length, items.length);
        items.push(items[1]);
        items.splice(1, 1);
        let index = 0;
        for (const item of list) {
            const itemIndex = index++;
            assert.strictEqual(item, items[itemIndex]);
        }
        assert.strictEqual(index, items.length);
        list.append(items[items.length - 1]);
        index = 0;
        for (const item of list) {
            const itemIndex = index++;
            assert.strictEqual(item, items[itemIndex]);
        }
        assert.strictEqual(index, items.length);
    });
    test('prepend()', function () {
        const items = [{}, {}, 3, 'test', {}];
        const list = new OrderedSet();
        for (const item of items) {
            list.prepend(item);
        }
        assert.strictEqual(list.length, items.length);
        items.reverse();
        for (let i = 0; i < list.length; ++i) {
            assert.strictEqual(list[i], items[i]);
        }
        list.prepend(items[1]);
        {
            const item = items[1];
            items.splice(1, 1);
            items.unshift(item);
        }
        let index = 0;
        for (const item of list) {
            const itemIndex = index++;
            assert.strictEqual(item, items[itemIndex]);
        }
        assert.strictEqual(index, items.length);
        list.prepend(items[0]);
        index = 0;
        for (const item of list) {
            const itemIndex = index++;
            assert.strictEqual(item, items[itemIndex]);
        }
        assert.strictEqual(index, items.length);
    });
    test('insert()', function () {
        const items = [{}, {}, 3, 'test', {}];
        const list = new OrderedSet();
        for (const item of items) {
            list.append(item);
        }
        assert.strictEqual(list.length, items.length);
        list.insert(1, 'foo');
        items.splice(1, 0, 'foo');
        assert.strictEqual(list.length, items.length);
        for (let i = 0; i < list.length; ++i) {
            assert.strictEqual(list[i], items[i]);
        }
        list.insert(4, items[3]);
        {
            const item = items[3];
            items.splice(3, 1);
            items.splice(4, 0, item);
        }
        let index = 0;
        for (const item of list) {
            const itemIndex = index++;
            assert.strictEqual(item, items[itemIndex]);
        }
        assert.strictEqual(index, items.length);
        list.insert(2, items[2]);
        index = 0;
        for (const item of list) {
            const itemIndex = index++;
            assert.strictEqual(item, items[itemIndex]);
        }
        assert.strictEqual(index, items.length);
        list.insert(-2, items[1]);
        {
            const item = items[1];
            const index = items.length - 2;
            items.splice(1, 1);
            items.splice(index, 0, item);
        }
        index = 0;
        for (const item of list) {
            const itemIndex = index++;
            assert.strictEqual(item, items[itemIndex]);
        }
        assert.strictEqual(index, items.length);
        list.insert(-200, items[4]);
        {
            const item = items[4];
            items.splice(4, 1);
            items.unshift(item);
        }
        index = 0;
        for (const item of list) {
            const itemIndex = index++;
            assert.strictEqual(item, items[itemIndex]);
        }
        assert.strictEqual(index, items.length);
        list.insert('not-a-number', items[3]);
        {
            const item = items[3];
            items.splice(3, 1);
            items.unshift(item);
        }
        index = 0;
        for (const item of list) {
            const itemIndex = index++;
            assert.strictEqual(item, items[itemIndex]);
        }
        assert.strictEqual(index, items.length);
        list.insert(100, items[2]);
        {
            const item = items[2];
            items.splice(2, 1);
            items.push(item);
        }
        index = 0;
        for (const item of list) {
            const itemIndex = index++;
            assert.strictEqual(item, items[itemIndex]);
        }
        assert.strictEqual(index, items.length);
    });
    test('item[]', function () {
        const items = [{}, {}, {}];
        const other = {};
        const set = new OrderedSet();
        for (const item of items) {
            set.append(item);
        }
        assert.strictEqual(set.item(0), items[0]);
        assert.strictEqual(set.item(1), items[1]);
        assert.strictEqual(set.item(2), items[2]);
        assert.strictEqual(set.item(3), null);
        assert.strictEqual(set.item(-1), null);
        assert.strictEqual(set.item(''), null);
        assert.strictEqual(set.item('length'), null);
        assert.strictEqual(set.item('item'), null);
        assert.strictEqual(set.item({}), null);
        assert.strictEqual(set.item([]), null);
        assert.strictEqual(set.item(), null);
        assert.throws(() => { set.item(Symbol('test')); }, TypeError);
        assert.strictEqual(set.indexOf(items[0]), 0);
        assert.strictEqual(set.indexOf(items[1]), 1);
        assert.strictEqual(set.indexOf(items[2]), 2);
        assert.strictEqual(set.lastIndexOf(items[0]), 0);
        assert.strictEqual(set.lastIndexOf(items[1]), 1);
        assert.strictEqual(set.lastIndexOf(items[2]), 2);
        assert.strictEqual(set[0], items[0]);
        assert.strictEqual(set[1], items[1]);
        assert.strictEqual(set[2], items[2]);
        assert.strictEqual(set[-1], undefined);
        assert.strictEqual(set[3], undefined);
        assert.strictEqual(0 in set, true);
        assert.strictEqual(1 in set, true);
        assert.strictEqual(2 in set, true);
        assert.strictEqual('1' in set, true);
        assert.strictEqual('0x1' in set, false);
        assert.strictEqual('01' in set, false);
        assert.strictEqual(Object.hasOwnProperty.call(set, 0), false);
        assert.strictEqual(Object.hasOwnProperty.call(set, 1), false);
        assert.strictEqual(Object.hasOwnProperty.call(set, 2), false);
        set[1] = other;
        assert.strictEqual(set.length, 3);
        assert.strictEqual(set[0], items[0]);
        assert.strictEqual(set[1], other);
        assert.strictEqual(set[2], items[2]);
        assert.throws(() => { set[3] = 5; }, RangeError);
        assert.doesNotThrow(() => { set[-1] = 5; });
        assert.doesNotThrow(() => { set['01'] = 5; });
        assert.strictEqual(set[1], other);
    });
    suite('replace()', () => {
        test('item not inside, replacement not inside, alone', function () {
            const item = {};
            const replacement = {};
            const set = new OrderedSet();
            assert.strictEqual(set.contains(item), false);
            assert.strictEqual(set.contains(replacement), false);
            assert.strictEqual(set.length, 0);
            set.replace(item, replacement);
            assert.strictEqual(set.contains(item), false);
            assert.strictEqual(set.contains(replacement), false);
            assert.strictEqual(set.length, 0);
        });
        test('item inside, replacement not inside, alone', function () {
            const item = {};
            const replacement = {};
            const set = new OrderedSet();
            set.append(item);
            assert.strictEqual(set.contains(item), true);
            assert.strictEqual(set.contains(replacement), false);
            assert.strictEqual(set.length, 1);
            set.replace(item, replacement);
            assert.strictEqual(set.contains(item), false);
            assert.strictEqual(set.contains(replacement), true);
            assert.strictEqual(set.length, 1);
        });
        test('item not inside, replacement inside, alone', function () {
            const item = {};
            const replacement = {};
            const set = new OrderedSet();
            set.append(replacement);
            assert.strictEqual(set.contains(item), false);
            assert.strictEqual(set.contains(replacement), true);
            assert.strictEqual(set.length, 1);
            set.replace(item, replacement);
            assert.strictEqual(set.contains(item), false);
            assert.strictEqual(set.contains(replacement), true);
            assert.strictEqual(set.length, 1);
        });
        test('item inside, replacement inside, alone', function () {
            const item = {};
            const replacement = {};
            const set = new OrderedSet();
            set.append(item);
            set.append(replacement);
            assert.strictEqual(set.contains(item), true);
            assert.strictEqual(set.contains(replacement), true);
            assert.strictEqual(set.length, 2);
            set.replace(item, replacement);
            assert.strictEqual(set.contains(item), false);
            assert.strictEqual(set.contains(replacement), true);
            assert.strictEqual(set.length, 1);
        });
        test('with other items, item before replacement', function () {
            const item = {};
            const replacement = {};
            const others = [[{}, {}, {}], [{}, {}, {}], [{}, {}, {}]];
            const set = new OrderedSet();
            for (const other of others[0]) {
                set.append(other);
            }
            set.append(item);
            for (const other of others[1]) {
                set.append(other);
            }
            set.append(replacement);
            for (const other of others[2]) {
                set.append(other);
            }
            assert.strictEqual(set.contains(item), true);
            assert.strictEqual(set.contains(replacement), true);
            assert.strictEqual(set.item(3), item);
            assert.strictEqual(set[3], item);
            assert.strictEqual(set.item(7), replacement);
            assert.strictEqual(set[7], replacement);
            assert.strictEqual(set.length, 11);
            set.replace(item, replacement);
            assert.strictEqual(set.contains(item), false);
            assert.strictEqual(set.contains(replacement), true);
            assert.strictEqual(set.item(3), replacement);
            assert.strictEqual(set[3], replacement);
            assert.notStrictEqual(set.item(7), replacement);
            assert.notStrictEqual(set[7], replacement);
            assert.strictEqual(set.length, 10);
        });
    });
    test('remove()', function () {
        const items = [{}, {}, {}];
        const other = {};
        const set = new OrderedSet();
        for (const item of items) {
            set.append(item);
        }
        assert.strictEqual(set.length, 3);
        assert.strictEqual(set.contains(items[1]), true);
        assert.strictEqual(set.contains(other), false);
        assert.strictEqual(set.item(0), items[0]);
        assert.strictEqual(set[0], items[0]);
        assert.strictEqual(set.item(1), items[1]);
        assert.strictEqual(set[1], items[1]);
        assert.strictEqual(set.item(2), items[2]);
        assert.strictEqual(set[2], items[2]);
        set.remove(items[1]);
        assert.strictEqual(set.length, 2);
        assert.strictEqual(set.contains(items[1]), false);
        assert.strictEqual(set.item(0), items[0]);
        assert.strictEqual(set[0], items[0]);
        assert.strictEqual(set.item(1), items[2]);
        assert.strictEqual(set[1], items[2]);
        assert.strictEqual(set.contains(other), false);
        set.remove(other);
        assert.strictEqual(set.length, 2);
        assert.strictEqual(set.contains(items[1]), false);
        assert.strictEqual(set.item(0), items[0]);
        assert.strictEqual(set[0], items[0]);
        assert.strictEqual(set.item(1), items[2]);
        assert.strictEqual(set[1], items[2]);
        assert.strictEqual(set.contains(other), false);
    });
    test('isSubsetOf(), isSupersetOf()', function () {
        const items = [{}, {}, {}];
        const other = [{}, {}];
        const set1 = new OrderedSet();
        assert.strictEqual(set1.isSubsetOf(set1), true);
        assert.strictEqual(set1.isSupersetOf(set1), true);
        const set2 = new OrderedSet();
        set1.append(items[0]);
        set1.append(items[1]);
        set1.append(items[2]);
        assert.strictEqual(set2.isSubsetOf(set1), true);
        assert.strictEqual(set1.isSupersetOf(set2), true);
        assert.strictEqual(set1.isSubsetOf(set2), false);
        assert.strictEqual(set2.isSupersetOf(set1), false);
        set2.append(items[1]);
        assert.strictEqual(set2.isSubsetOf(set1), true);
        assert.strictEqual(set1.isSupersetOf(set2), true);
        assert.strictEqual(set1.isSubsetOf(set2), false);
        assert.strictEqual(set2.isSupersetOf(set1), false);
        set2.append(other[1]);
        assert.strictEqual(set2.isSubsetOf(set1), false);
        assert.strictEqual(set1.isSupersetOf(set2), false);
        assert.strictEqual(set1.isSubsetOf(set2), false);
        assert.strictEqual(set2.isSupersetOf(set1), false);
    });
    test('range()', function () {
        const items = [];
        const set = new OrderedSet();
        for (let i = 0; i < 12; ++i) {
            items.push({});
            set.append(items[i]);
        }
        let i = 4;
        for (const item of set.range(4, 8)) {
            const index = i++;
            assert.strictEqual(item, items[index]);
            assert.isBelow(index, 8);
        }
        assert.strictEqual(i, 8);
        i = 6;
        for (const item of set.range(6)) {
            const index = i++;
            assert.strictEqual(item, items[index]);
            assert.isBelow(index, items.length);
        }
        assert.strictEqual(i, items.length);
        i = 0;
        for (const item of set.range()) {
            const index = i++;
            assert.strictEqual(item, items[index]);
            assert.isBelow(index, items.length);
        }
        assert.strictEqual(i, items.length);
        i = 8;
        // eslint-disable-next-line no-unused-vars
        for (const item of set.range(8, 4)) {
            i--;
        }
        assert.strictEqual(i, 8);
    });
    test('entries()', function () {
        const items = [];
        const set = new OrderedSet();
        for (let i = 0; i < 10; ++i) {
            const item = {};
            items.push(item);
            set.append(item);
            assert.strictEqual(set.contains(item), true);
        }
        let i = 0;
        for (const [key, value] of set.entries()) {
            const index = i++;
            assert.strictEqual(key, value);
            assert.strictEqual(key, items[index]);
        }
    });
    test('clear()', function () {
        const items = [];
        const set = new OrderedSet();
        for (let i = 0; i < 10; ++i) {
            const item = {};
            items.push(item);
            set.append(item);
            assert.strictEqual(set.contains(item), true);
        }
        assert.strictEqual(set.length, 10);
        set.clear();
        assert.strictEqual(set.length, 0);
        for (const item of items) {
            assert.strictEqual(set.contains(item), false);
        }
    });
    test('clone()', function () {
        const items = [{}, {}, {}, {}, {}];
        const set = new OrderedSet();
        for (let i = 0; i < 3; ++i) {
            set.append(items[i]);
        }
        assert.strictEqual(set.length, 3);
        const clone = set.clone();
        set.prepend(items[3]);
        set.append(items[4]);
        assert.strictEqual(set.length, 5);
        assert.strictEqual(clone.length, 3);
        for (let i = 0; i < 3; ++i) {
            assert.strictEqual(clone.item(i), items[i]);
            assert.strictEqual(clone.contains(items[i]), true);
            assert.strictEqual(clone[i], items[i]);
        }
    });
    test('fromIterable()', function () {
        const array = [{}, {}, {}];
        array.push(array[1]);
        const set = OrderedSet.fromIterable(arrayIterate(array));
        assert.strictEqual(set.length, 3);
        for (let i = 0; i < 3; ++i) {
            assert.strictEqual(set.item(i), array[i]);
        }
        assert.strictEqual(array[3], set.item(1));

        function * arrayIterate(arr) {
            for (let i = 0; i < arr.length; i++) {
                yield arr[i];
            }
        }
    });
    test('from(...)', function () {
        const array = [{}, {}, {}];
        array.push(array[1]);
        const set = OrderedSet.from(...array);
        assert.strictEqual(set.length, 3);
        for (let i = 0; i < 3; ++i) {
            assert.strictEqual(set.item(i), array[i]);
        }
        assert.strictEqual(array[3], set.item(1));
    });
    test('[Symbol.iterator]', function () {
        const array = [{}, {}, {}, {}, {}];
        const set = OrderedSet.from(...array);
        assert.typeOf(set[Symbol.iterator], 'function');
        assert.strictEqual(set.length, array.length);
        for (let i = 0; i < 3; ++i) {
            let iterations = 0;
            for (const value of set) {
                assert.strictEqual(value, array[iterations]);
                iterations++;
            }
            assert.strictEqual(iterations, array.length);
        }
        for (let i = 0; i < 3; ++i) {
            let iterations = 0;
            for (const value of set.keys()) {
                assert.strictEqual(value, array[iterations]);
                iterations++;
            }
            assert.strictEqual(iterations, array.length);
        }
        for (let i = 0; i < 3; ++i) {
            let iterations = 0;
            for (const value of set.values()) {
                assert.strictEqual(value, array[iterations]);
                iterations++;
            }
            assert.strictEqual(iterations, array.length);
        }
        for (let i = 0; i < 3; ++i) {
            let iterations = 0;
            for (const [key, value] of set.entries()) {
                assert.strictEqual(value, key);
                assert.strictEqual(value, array[iterations]);
                iterations++;
            }
            assert.strictEqual(iterations, array.length);
        }
    });
});
