import { assert } from 'chai';
import { suite, test } from 'mocha';
import OrderedMap from '../src/ordered-map.js';

suite('OrderedMap', () => {
    test('class', function () {
        assert(typeof OrderedMap === 'function' && OrderedMap.prototype != null, `OrderedMap should be a class/constructor function`);
    });
    test('new', function () {
        let orderedMap;
        assert.doesNotThrow(() => {
            orderedMap = new OrderedMap();
        });
        assert.strictEqual(orderedMap.length, 0);
        const symbol = Symbol('test');
        assert.doesNotThrow(() => {
            'use strict';
            orderedMap[symbol] = true;
            assert(symbol in orderedMap, `Index handler should not affect symbol properties`);
        });
        assert.equal(Object.prototype.toString.call(orderedMap), '[object OrderedMap]');
    });
    test('append()', function () {
        const items = [{}, {}, 3, 'test', {}];
        const values = [30, 18, 100, 43, 11];
        const other = 15;
        const map = new OrderedMap();
        for (let i = 0; i < items.length; ++i) {
            map.append(items[i], values[i]);
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
        }
        map.append(items[1], other);
        {
            const item = items[1];
            items.splice(1, 1);
            items.push(item);
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
        }
    });
    test('prepend()', function () {
        const items = [{}, {}, 3, 'test', {}];
        const values = [30, 18, 100, 43, 11];
        const other = 15;
        const map = new OrderedMap();
        for (let i = 0; i < items.length; ++i) {
            map.append(items[i], values[i]);
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
        }
        map.prepend(items[1], other);
        {
            const item = items[1];
            items.splice(1, 1);
            items.unshift(item);
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
        }
    });
    test('insert()', function () {
        const items = [{}, {}, 3, 'test', {}];
        const values = [11, 22, 33, 44, 55];
        const map = new OrderedMap();
        for (let i = 0; i < items.length; ++i) {
            map.append(items[i], values[i]);
        }
        assert.strictEqual(map.length, items.length);
        {
            const item = 'foo';
            const value = 'bar';
            map.insert(1, item, value);
            items.splice(1, 0, item);
            values.splice(1, 0, value);
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
            assert.strictEqual(map.item(i), items[i]);
            assert.strictEqual(map.value(i), values[i]);
            assert.strictEqual(map.get(items[i]), values[i]);
        }
        {
            const item = items[3];
            const value = 15;
            map.insert(4, items[3], value);
            items.splice(3, 1);
            items.splice(4, 0, item);
            values.splice(3, 1);
            values.splice(4, 0, value);
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
            assert.strictEqual(map.item(i), items[i]);
            assert.strictEqual(map.value(i), values[i]);
            assert.strictEqual(map.get(items[i]), values[i]);
        }
        {
            const value = 28;
            map.insert(2, items[2], value);
            values[2] = value;
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
            assert.strictEqual(map.item(i), items[i]);
            assert.strictEqual(map.value(i), values[i]);
            assert.strictEqual(map.get(items[i]), values[i]);
        }
        {
            const item = items[1];
            const value = 37;
            map.insert(-2, items[1], value);
            const index = items.length - 2;
            items.splice(1, 1);
            items.splice(index, 0, item);
            values.splice(1, 1);
            values.splice(index, 0, value);
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
            assert.strictEqual(map.item(i), items[i]);
            assert.strictEqual(map.value(i), values[i]);
            assert.strictEqual(map.get(items[i]), values[i]);
        }
        {
            const item = items[4];
            const value = 42;
            map.insert(-200, items[4], value);
            items.splice(4, 1);
            items.unshift(item);
            values.splice(4, 1);
            values.unshift(value);
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
            assert.strictEqual(map.item(i), items[i]);
            assert.strictEqual(map.value(i), values[i]);
            assert.strictEqual(map.get(items[i]), values[i]);
        }
        {
            const item = items[3];
            const value = 59;
            map.insert('not-a-number', items[3], value);
            items.splice(3, 1);
            items.unshift(item);
            values.splice(3, 1);
            values.unshift(value);
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
            assert.strictEqual(map.item(i), items[i]);
            assert.strictEqual(map.value(i), values[i]);
            assert.strictEqual(map.get(items[i]), values[i]);
        }
        {
            const item = items[2];
            const value = 69;
            map.insert(100, items[2], value);
            items.splice(2, 1);
            items.push(item);
            values.splice(2, 1);
            values.push(value);
        }
        assert.strictEqual(map.length, items.length);
        for (let i = 0; i < map.length; ++i) {
            assert.strictEqual(map[i], items[i]);
            assert.strictEqual(map.item(i), items[i]);
            assert.strictEqual(map.value(i), values[i]);
            assert.strictEqual(map.get(items[i]), values[i]);
        }
    });
    test('index[]', function () {
        const items = [{}, {}, {}];
        const values = [11, 22, 33];
        const other = {};
        const map = new OrderedMap();
        for (let i = 0; i < items.length; ++i) {
            map.set(items[i], values[i]);
        }
        assert.strictEqual(map.item(0), items[0]);
        assert.strictEqual(map.item(1), items[1]);
        assert.strictEqual(map.item(2), items[2]);
        assert.strictEqual(map.item(3), undefined);
        assert.strictEqual(map.item(-1), undefined);
        assert.strictEqual(map.item(''), undefined);
        assert.strictEqual(map.item('length'), undefined);
        assert.strictEqual(map.item('item'), undefined);
        assert.strictEqual(map.item({}), undefined);
        assert.strictEqual(map.item([]), undefined);
        assert.strictEqual(map.item(), undefined);
        assert.throws(() => { map.item(Symbol('test')); }, TypeError, 'Cannot convert a Symbol value to a number');
        assert.strictEqual(map.value(0), values[0]);
        assert.strictEqual(map.value(1), values[1]);
        assert.strictEqual(map.value(2), values[2]);
        assert.strictEqual(map.value(3), undefined);
        assert.strictEqual(map.value(-1), undefined);
        assert.strictEqual(map.value(''), undefined);
        assert.strictEqual(map.value('length'), undefined);
        assert.strictEqual(map.value('item'), undefined);
        assert.strictEqual(map.value({}), undefined);
        assert.strictEqual(map.value([]), undefined);
        assert.strictEqual(map.value(), undefined);
        assert.strictEqual(map.indexOf(items[0]), 0);
        assert.strictEqual(map.indexOf(items[1]), 1);
        assert.strictEqual(map.indexOf(items[2]), 2);
        assert.strictEqual(map.lastIndexOf(items[0]), 0);
        assert.strictEqual(map.lastIndexOf(items[1]), 1);
        assert.strictEqual(map.lastIndexOf(items[2]), 2);
        let index = 0;
        for (const key of map.keys()) {
            assert.strictEqual(key, items[index++]);
        }
        index = 0;
        for (const value of map.values()) {
            assert.strictEqual(value, values[index++]);
        }
        index = 0;
        for (const [key, value] of map.entries()) {
            const itemIndex = index++;
            assert.strictEqual(key, items[itemIndex]);
            assert.strictEqual(value, values[itemIndex]);
        }
        assert.strictEqual(map.get(items[0]), values[0]);
        assert.strictEqual(map.get(items[1]), values[1]);
        assert.strictEqual(map.get(items[2]), values[2]);
        assert.strictEqual(map.get(Symbol('test')), undefined);
        assert.strictEqual(map.get({}), undefined);
        assert.strictEqual(map.get(null), undefined);
        assert.strictEqual(map.get(true), undefined);
        assert.strictEqual(map.get(undefined), undefined);
        assert.strictEqual(map.get(), undefined);
        assert.strictEqual(map[0], items[0]);
        assert.strictEqual(map[1], items[1]);
        assert.strictEqual(map[2], items[2]);
        assert.strictEqual(map[-1], undefined);
        assert.strictEqual(map[3], undefined);
        assert.strictEqual(0 in map, true);
        assert.strictEqual(1 in map, true);
        assert.strictEqual(2 in map, true);
        assert.strictEqual('1' in map, true);
        assert.strictEqual('0x1' in map, false);
        assert.strictEqual('01' in map, false);
        assert.strictEqual(Object.hasOwnProperty.call(map, 0), false);
        assert.strictEqual(Object.hasOwnProperty.call(map, 1), false);
        assert.strictEqual(Object.hasOwnProperty.call(map, 2), false);
        const value1 = map.value(1);
        assert.doesNotThrow(() => { map[1] = other; });
        assert.strictEqual(map.length, 3);
        assert.strictEqual(map[0], items[0]);
        assert.strictEqual(map[1], other);
        assert.strictEqual(map.get(other), value1);
        assert.strictEqual(map[2], items[2]);
        assert.throws(() => { map[3] = 5; }, RangeError);
        assert.throws(() => { map[100] = 5; }, RangeError);
        assert.doesNotThrow(() => { map[-1] = 5; });
        assert.doesNotThrow(() => { map['012'] = 5; });
        assert.doesNotThrow(() => { map[8.5] = 5; });
        assert.strictEqual(map[1], other);
    });
    test('range()', function () {
        const items = [];
        const values = [];
        const map = new OrderedMap();
        for (let i = 0; i < 12; ++i) {
            items.push({});
            values.push(i);
            map.set(items[i], values[i]);
        }
        let i = 4;
        for (const [item, value] of map.range(4, 8)) {
            const index = i++;
            assert.strictEqual(value, values[index]);
            assert.strictEqual(item, items[index]);
            assert.isBelow(index, 8);
        }
        assert.strictEqual(i, 8);
        i = 6;
        for (const [item, value] of map.range(6)) {
            const index = i++;
            assert.strictEqual(value, values[index]);
            assert.strictEqual(item, items[index]);
            assert.isBelow(index, values.length);
        }
        assert.strictEqual(i, values.length);
        i = 0;
        for (const [item, value] of map.range()) {
            const index = i++;
            assert.strictEqual(value, values[index]);
            assert.strictEqual(item, items[index]);
            assert.isBelow(index, values.length);
        }
        assert.strictEqual(i, values.length);
        i = 8;
        // eslint-disable-next-line no-unused-vars
        for (const [item, value] of map.range(8, 4)) {
            i--;
        }
        assert.strictEqual(i, 8);
    });
    test('remove()', function () {
        const items = [{}, {}, {}];
        const values = [11, 22, 33];
        const other = {};
        const set = new OrderedMap();
        for (let i = 0; i < items.length; ++i) {
            set.append(items[i], values[i]);
        }
        assert.strictEqual(set.length, 3);
        assert.strictEqual(set.contains(items[1]), true);
        assert.strictEqual(set.contains(other), false);
        assert.strictEqual(set.item(0), items[0]);
        assert.strictEqual(set.value(0), values[0]);
        assert.strictEqual(set[0], items[0]);
        assert.strictEqual(set.item(1), items[1]);
        assert.strictEqual(set.value(1), values[1]);
        assert.strictEqual(set[1], items[1]);
        assert.strictEqual(set.item(2), items[2]);
        assert.strictEqual(set.value(2), values[2]);
        assert.strictEqual(set[2], items[2]);
        set.remove(items[1]);
        assert.strictEqual(set.length, 2);
        assert.strictEqual(set.contains(items[1]), false);
        assert.strictEqual(set.item(0), items[0]);
        assert.strictEqual(set.value(0), values[0]);
        assert.strictEqual(set[0], items[0]);
        assert.strictEqual(set.item(1), items[2]);
        assert.strictEqual(set.value(1), values[2]);
        assert.strictEqual(set[1], items[2]);
        assert.strictEqual(set.contains(other), false);
        set.remove(other);
        assert.strictEqual(set.length, 2);
        assert.strictEqual(set.contains(items[1]), false);
        assert.strictEqual(set.item(0), items[0]);
        assert.strictEqual(set.value(0), values[0]);
        assert.strictEqual(set[0], items[0]);
        assert.strictEqual(set.item(1), items[2]);
        assert.strictEqual(set.value(1), values[2]);
        assert.strictEqual(set[1], items[2]);
        assert.strictEqual(set.contains(other), false);
    });
    test('clear', function () {
        const items = [];
        const values = [];
        const map = new OrderedMap();
        for (let i = 0; i < 10; ++i) {
            const item = {};
            const value = Math.floor(Math.random() * 1000);
            items.push(item);
            values.push(value);
            map.append(item, value);
            assert.strictEqual(map.contains(item), true);
            assert.strictEqual(map.item(i), items[i]);
            assert.strictEqual(map.value(i), values[i]);
            assert.strictEqual(map[i], items[i]);
        }
        assert.strictEqual(map.length, 10);
        map.clear();
        assert.strictEqual(map.length, 0);
        for (let i = 0; i < 10; ++i) {
            assert.strictEqual(map.contains(items[i]), false);
            assert.strictEqual(map[i], undefined);
        }
    });
    test('clone()', function () {
        const items = [{}, {}, {}, {}, {}];
        const values = [11, items[3], items[3], 44, items[3]];
        const map = new OrderedMap();
        for (let i = 0; i < 3; ++i) {
            map.set(items[i], values[i]);
        }
        assert.strictEqual(map.length, 3);
        const clone = map.clone();
        map.prepend(items[3], values[3]);
        map.append(items[4], values[4]);
        assert.strictEqual(map.length, 5);
        assert.strictEqual(clone.length, 3);
        for (let i = 0; i < 3; ++i) {
            assert.strictEqual(clone.item(i), items[i]);
            assert.strictEqual(clone.value(i), values[i]);
            assert.strictEqual(clone.get(items[i]), values[i]);
            assert.strictEqual(clone[i], items[i]);
        }
    });
    test('sort()', function () {
        const items = [5, 1, 8, 11, 4];
        const map = new OrderedMap();
        for (let i = 0; i < items.length; ++i) {
            map.set(items[i], { test: items[i] });
        }
        assert.strictEqual(map.length, items.length);
        const sortReturn = map.sort();
        assert.strictEqual(sortReturn, map);
        assert.strictEqual(map.length, items.length);
        const sorted = items.slice(0).sort();
        for (let i = 0; i < items.length; ++i) {
            assert.strictEqual(map.item(i), sorted[i]);
            const value = map.get(sorted[i]);
            assert(value != null && typeof value === 'object', `value != null && typeof value === 'object'`);
            assert.strictEqual(value.test, sorted[i]);
        }
    });
    test('sort(callback)', function () {
        const items = [5, 1, 8, 11, 4];
        const map = new OrderedMap();
        for (let i = 0; i < items.length; ++i) {
            map.set(items[i], { test: items[i] });
        }
        const sorter = (a, b) => {
            return b - a;
        };
        assert.strictEqual(map.length, items.length);
        const sortReturn = map.sort(sorter);
        assert.strictEqual(sortReturn, map);
        assert.strictEqual(map.length, items.length);
        const sorted = items.slice(0).sort(sorter);
        for (let i = 0; i < items.length; ++i) {
            assert.strictEqual(map.item(i), sorted[i]);
            const value = map.get(sorted[i]);
            assert(value != null && typeof value === 'object', `value != null && typeof value === 'object'`);
            assert.strictEqual(value.test, sorted[i]);
        }
    });
    test('[Symbol.iterator]', function () {
        const keyList = [{}, {}, {}, {}, {}];
        const valueList = [{}, {}, {}, {}, {}];
        const map = new OrderedMap();
        assert.typeOf(map[Symbol.iterator], 'function');
        for (let i = 0; i < keyList.length; ++i) {
            map.append(keyList[i], valueList[i]);
        }
        assert.strictEqual(map.length, keyList.length);
        for (let i = 0; i < 3; ++i) {
            let iterations = 0;
            for (const [key, value] of map) {
                assert.strictEqual(key, keyList[iterations]);
                assert.strictEqual(value, valueList[iterations]);
                iterations++;
            }
            assert.strictEqual(iterations, keyList.length);
        }
        for (let i = 0; i < 3; ++i) {
            let iterations = 0;
            for (const key of map.keys()) {
                assert.strictEqual(key, keyList[iterations]);
                iterations++;
            }
            assert.strictEqual(iterations, keyList.length);
        }
        for (let i = 0; i < 3; ++i) {
            let iterations = 0;
            for (const value of map.values()) {
                assert.strictEqual(value, valueList[iterations]);
                iterations++;
            }
            assert.strictEqual(iterations, keyList.length);
        }
        for (let i = 0; i < 3; ++i) {
            let iterations = 0;
            for (const [key, value] of map.entries()) {
                assert.strictEqual(key, keyList[iterations]);
                assert.strictEqual(value, valueList[iterations]);
                iterations++;
            }
            assert.strictEqual(iterations, keyList.length);
        }
    });
});
