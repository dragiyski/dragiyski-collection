import { assert } from 'chai';
import { suite, test } from 'mocha';
import Implementation from '../src/implementation.js';

suite('Implementation', function () {
    test('is class', function () {
        assert(typeof Implementation === 'function' && Implementation.prototype != null);
    });
    test('new', function () {
        let implementation;
        assert.doesNotThrow(() => {
            implementation = new Implementation();
        });
        assert.equal(Object.prototype.toString.call(implementation), '[object Implementation]');
    });
    test('getOwnInterface(), getOwnImplementation()', function () {
        const implementation = new Implementation();
        const test = {
            interface: Object.create(null),
            implementation: Object.create(null)
        };
        const linkReturn = implementation.setImplementation(test.interface, test.implementation);
        assert.strictEqual(linkReturn, implementation);
        // The interface should have no additional assigned properties, symbols and its prototype must be unchanged (remain null).
        assert.strictEqual(Object.getOwnPropertyNames(test.interface).length, 0);
        assert.strictEqual(Object.getOwnPropertySymbols(test.interface).length, 0);
        assert.strictEqual(Object.getPrototypeOf(test.interface), null);
        // Even no properties, symbols and prototype is assigned, we should be able to retrieve the implementation from the interface object.
        assert.strictEqual(implementation.hasOwnImplementation(test.interface), true);
        assert.strictEqual(implementation.getOwnImplementation(test.interface), test.implementation);
        assert.strictEqual(implementation.hasOwnInterface(test.implementation), true);
        assert.strictEqual(implementation.getOwnInterface(test.implementation), test.interface);
    });
    test('getInterface(), getImplementation()', function () {
        const implementation = new Implementation();
        const test = {
            baseInterface: Object.create(null),
            baseImplementation: Object.create(null),
            unrelated: Object.create(null)
        };
        test.childInterface = Object.create(test.baseInterface);
        test.childImplementation = Object.create(test.baseImplementation);
        implementation.setImplementation(test.baseInterface, test.baseImplementation);
        assert.strictEqual(Object.getOwnPropertyNames(test.baseInterface).length, 0);
        assert.strictEqual(Object.getOwnPropertySymbols(test.baseInterface).length, 0);
        assert.strictEqual(Object.getPrototypeOf(test.baseInterface), null);
        assert.strictEqual(implementation.hasOwnImplementation(test.baseInterface), true);
        assert.strictEqual(implementation.getOwnImplementation(test.baseInterface), test.baseImplementation);
        assert.strictEqual(implementation.hasOwnInterface(test.baseImplementation), true);
        assert.strictEqual(implementation.getOwnInterface(test.baseImplementation), test.baseInterface);
        assert.strictEqual(implementation.hasOwnImplementation(test.childInterface), false);
        assert(implementation.getOwnImplementation(test.childInterface) == null);
        assert.strictEqual(implementation.hasOwnInterface(test.childImplementation), false);
        assert(implementation.getOwnInterface(test.childImplementation) == null);
        assert.strictEqual(implementation.hasImplementation(test.childInterface), true);
        assert.strictEqual(implementation.getImplementation(test.childInterface), test.baseImplementation);
        assert.strictEqual(implementation.hasInterface(test.childImplementation), true);
        assert.strictEqual(implementation.getInterface(test.childImplementation), test.baseInterface);
        assert.strictEqual(implementation.hasImplementation(test.unrelated), false);
        assert.strictEqual(implementation.hasInterface(test.unrelated), false);
        assert(implementation.getImplementation(test.unrelated) == null);
        assert(implementation.getInterface(test.unrelated) == null);
    });
    test('getImplementation(primitive)', function () {
        const implementation = new Implementation();
        const interfaces = [
            {},
            {
                toString: function () {
                    return test;
                }
            }
        ];
        for (const iface of interfaces) {
            implementation.setImplementation(iface, iface);
        }
        assert.doesNotThrow(() => {
            implementation.getImplementation(null);
        });
        assert.doesNotThrow(() => {
            implementation.getOwnImplementation(null);
        });
        assert.doesNotThrow(() => {
            implementation.getImplementation(true);
        });
        assert.doesNotThrow(() => {
            implementation.getOwnImplementation(true);
        });
        assert.doesNotThrow(() => {
            implementation.getImplementation(false);
        });
        assert.doesNotThrow(() => {
            implementation.getOwnImplementation(false);
        });
        assert.doesNotThrow(() => {
            implementation.getImplementation(2);
        });
        assert.doesNotThrow(() => {
            implementation.getOwnImplementation(2);
        });
        assert.doesNotThrow(() => {
            implementation.getImplementation('test');
        });
        assert.doesNotThrow(() => {
            implementation.getOwnImplementation('test');
        });
        assert.doesNotThrow(() => {
            implementation.getImplementation(Symbol('test'));
        });
        assert.doesNotThrow(() => {
            implementation.getOwnImplementation(Symbol('test'));
        });
        assert.doesNotThrow(() => {
            implementation.getInterface(null);
        });
        assert.doesNotThrow(() => {
            implementation.getOwnInterface(null);
        });
        assert.doesNotThrow(() => {
            implementation.getInterface(true);
        });
        assert.doesNotThrow(() => {
            implementation.getOwnInterface(true);
        });
        assert.doesNotThrow(() => {
            implementation.getInterface(false);
        });
        assert.doesNotThrow(() => {
            implementation.getOwnInterface(false);
        });
        assert.doesNotThrow(() => {
            implementation.getInterface(2);
        });
        assert.doesNotThrow(() => {
            implementation.getOwnInterface(2);
        });
        assert.doesNotThrow(() => {
            implementation.getInterface('test');
        });
        assert.doesNotThrow(() => {
            implementation.getOwnInterface('test');
        });
        assert.doesNotThrow(() => {
            implementation.getInterface(Symbol('test'));
        });
        assert.doesNotThrow(() => {
            implementation.getOwnInterface(Symbol('test'));
        });
        assert.strictEqual(implementation.hasInterface(null), false);
        assert.strictEqual(implementation.hasOwnInterface(null), false);
        assert.strictEqual(implementation.hasImplementation(null), false);
        assert.strictEqual(implementation.hasOwnImplementation(null), false);

        assert.strictEqual(implementation.hasInterface(true), false);
        assert.strictEqual(implementation.hasOwnInterface(true), false);
        assert.strictEqual(implementation.hasImplementation(true), false);
        assert.strictEqual(implementation.hasOwnImplementation(true), false);

        assert.strictEqual(implementation.hasInterface(false), false);
        assert.strictEqual(implementation.hasOwnInterface(false), false);
        assert.strictEqual(implementation.hasImplementation(false), false);
        assert.strictEqual(implementation.hasOwnImplementation(false), false);

        assert.strictEqual(implementation.hasInterface(2), false);
        assert.strictEqual(implementation.hasOwnInterface(2), false);
        assert.strictEqual(implementation.hasImplementation(2), false);
        assert.strictEqual(implementation.hasOwnImplementation(2), false);

        assert.strictEqual(implementation.hasInterface('test'), false);
        assert.strictEqual(implementation.hasOwnInterface('test'), false);
        assert.strictEqual(implementation.hasImplementation('test'), false);
        assert.strictEqual(implementation.hasOwnImplementation('test'), false);

        assert.strictEqual(implementation.hasInterface(Symbol('test')), false);
        assert.strictEqual(implementation.hasOwnInterface(Symbol('test')), false);
        assert.strictEqual(implementation.hasImplementation(Symbol('test')), false);
        assert.strictEqual(implementation.hasOwnImplementation(Symbol('test')), false);
    });
    test('setImplementation()', function () {
        const interface1 = Object.create(null);
        const implementation1 = Object.create(null);
        const interface2 = Object.create(null);
        const implementation2 = Object.create(null);
        const collection = new Implementation();
        let thrown = false;
        try {
            collection.setImplementation('test', implementation1);
        } catch (e) {
            thrown = true;
            assert.strictEqual(e.name, 'TypeError');
        }
        assert.strictEqual(thrown, true);
        assert(Object.getPrototypeOf(implementation1) == null);
        assert.strictEqual(Object.keys(Object.getOwnPropertyDescriptors(implementation1)).length, 0);
        assert.strictEqual(collection.hasOwnInterface(implementation1), false);
        thrown = false;
        try {
            collection.setImplementation(interface1, 'test');
        } catch (e) {
            thrown = true;
            assert.strictEqual(e.name, 'TypeError');
        }
        assert.strictEqual(thrown, true);
        assert.strictEqual(collection.hasOwnImplementation(interface1), false);
        assert.doesNotThrow(() => { collection.setImplementation(interface1, implementation2); });
        assert(!collection.hasInterface(implementation1));
        assert(collection.hasInterface(implementation2));
        assert.strictEqual(collection.getImplementation(interface1), implementation2);
        assert.doesNotThrow(() => { collection.setImplementation(interface2, implementation2); });
        assert(!collection.hasImplementation(interface1));
        assert(collection.hasImplementation(interface2));
        assert.strictEqual(collection.getOwnImplementation(interface2), implementation2);
        assert.strictEqual(collection.getOwnInterface(implementation2), interface2);
        thrown = false;
        try {
            collection.setImplementation('test', implementation2);
        } catch (e) {
            thrown = true;
            assert.strictEqual(e.name, 'TypeError');
        }
        assert.strictEqual(thrown, true);
        assert.strictEqual(collection.getOwnImplementation(interface2), implementation2);
        assert.strictEqual(collection.getOwnInterface(implementation2), interface2);
        thrown = false;
        try {
            collection.setImplementation(interface2, 'test');
        } catch (e) {
            thrown = true;
            assert.strictEqual(e.name, 'TypeError');
        }
        assert.strictEqual(thrown, true);
        assert.strictEqual(collection.getOwnImplementation(interface2), implementation2);
        assert.strictEqual(collection.getOwnInterface(implementation2), interface2);
        assert.doesNotThrow(() => { collection.setImplementation(interface1, implementation1); });
        assert.strictEqual(collection.getOwnImplementation(interface1), implementation1);
        assert.strictEqual(collection.getOwnInterface(implementation1), interface1);
        assert.doesNotThrow(() => { collection.setImplementation(interface1, implementation2); });
        assert.strictEqual(collection.hasOwnImplementation(interface2), false);
        assert.strictEqual(collection.getOwnImplementation(interface1), implementation2);
        assert.strictEqual(collection.hasOwnInterface(implementation1), false);
        assert.strictEqual(collection.getOwnInterface(implementation2), interface1);
    });
    test('removeInterface(), removeImplementation()', function () {
        const interface1 = Object.create(null);
        const implementation1 = Object.create(null);
        const interface2 = Object.create(null);
        const implementation2 = Object.create(null);
        const collection = new Implementation();
        collection.setImplementation(interface1, implementation1);
        collection.setImplementation(interface2, implementation2);
        assert.strictEqual(collection.getOwnImplementation(interface1), implementation1);
        assert.strictEqual(collection.getOwnImplementation(interface2), implementation2);
        collection.removeInterface(interface1);
        assert.strictEqual(collection.hasOwnImplementation(interface1), false);
        assert.strictEqual(collection.hasOwnInterface(implementation1), false);
        collection.removeImplementation(implementation2);
        assert.strictEqual(collection.hasOwnImplementation(interface2), false);
        assert.strictEqual(collection.hasOwnInterface(implementation2), false);
        assert.doesNotThrow(() => { collection.removeInterface(interface2); });
        assert.doesNotThrow(() => { collection.removeImplementation(implementation1); });
    });
});
