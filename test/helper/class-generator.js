import { compileFunction } from 'vm';
import Mixin from '../../src/mixin.js';
import { assert } from 'chai';

export function generator(options) {
    const code = createTestTreeSource(unwrapOptions(options)).join('\n');
    return compileFunction(code, ['options'], {
        contextExtensions: [Object.assign(Object.create(null), { Mixin })]
    });
}

const ClassGeneratorError = generator.ClassGeneratorError = class ClassGeneratorError extends Error {};

function unwrapOptions(options) {
    options = { ...options };
    options.tree ??= {};
    options.methods ??= {};
    options.mixins ??= {};
    options.extends ??= {};
    options.indent ??= 0;
    const classMap = Object.create(null);
    const classList = [];
    for (const [className, parentName] of depthFirstTreeWalk(options.tree)) {
        const classOptions = {
            name: className,
            parentName,
            indent: options.indent,
            methods: Object.assign(Object.create(null), {
                static: Object.create(null),
                prototype: Object.create(null)
            }),
            classMap
        };
        classMap[className] = classOptions;
        classList.push(classOptions);
    }
    for (const classOptions of classList) {
        if (classOptions.parentName == null) {
            continue;
        }
        if (!(classOptions.parentName in classMap)) {
            throw new ClassGeneratorError(`Class '${classOptions.name}' refers to non-existing parent class '${classOptions.parentName}'`);
        }
        classOptions.parent = classMap[classOptions.parentName];
    }
    for (const mixinName of Object.keys(options.mixins)) {
        const rootName = options.mixins[mixinName];
        if (!(mixinName in classMap)) {
            throw new ClassGeneratorError(`Mixin '${mixinName}' specified, but no class with that name exists`);
        }
        if (!(rootName in classMap)) {
            throw new ClassGeneratorError(`Mixin '${mixinName}' specified '${rootName}' as root, but no class with that name exists`);
        }
        classMap[mixinName].mixinRoot = classMap[rootName];
    }
    for (const className of Object.keys(options.extends)) {
        if (!(className in classMap)) {
            throw new ClassGeneratorError(`Class '${className}' specified for extension does not exist`);
        }
        let mixinList = options.extends[className];
        if (!Array.isArray(mixinList)) {
            mixinList = [mixinList];
        }
        const classOptions = classMap[className];
        classOptions.mixins = Object.create(null);
        for (const mixinName of mixinList) {
            if (!(mixinName in classMap)) {
                throw new ClassGeneratorError(`Mixin '${mixinName}' specified in extends of class '${className}' does not exists`);
            }
            const mixinOptions = classMap[mixinName];
            if (mixinOptions.mixinRoot == null) {
                throw new ClassGeneratorError(`Mixin '${mixinName}' specified in extends of class '${className}' is not defined as mixin`);
            }
            classOptions.mixins[mixinName] = mixinOptions;
        }
    }
    for (const className of Object.keys(options.methods)) {
        if (!(className in classMap)) {
            throw new ClassGeneratorError(`Methods specified for class ${className}, but the class does not exist`);
        }
        const classOptions = classMap[className];
        const methods = options.methods[className];
        for (const type of ['static', 'prototype']) {
            if (methods[type] == null) {
                continue;
            }
            for (const methodName of Object.keys(methods[type])) {
                const methodOptions = Object.assign(Object.create(null), methods[type][methodName]);
                methodOptions.name = methodName;
                methodOptions.static = type === 'static';
                methodOptions.class = classMap[className];
                methodOptions.type = type;
                classOptions.methods[type][methodName] = methodOptions;
            }
        }
    }
    if (options.class != null) {
        for (const className of Object.keys(options.class)) {
            if (!(className in classMap)) {
                throw new ClassGeneratorError(`Options specified for class ${className}, but the class does not exist`);
            }
            const moreOptions = options.class[className];
            const classOptions = classMap[className];
            for (const name of Object.keys(moreOptions)) {
                if (Object.hasOwnProperty.call(classOptions, name)) {
                    throw new ClassGeneratorError(`Cannot set option ${name} specified for class ${className}: it will override essential option`);
                }
                classOptions[name] = moreOptions[name];
            }
        }
    }
    classList.indent = options.indent;
    return classList;
}

function * depthFirstTreeWalk(object, parentName = null) {
    for (const name of Object.keys(object)) {
        yield [name, parentName];
        yield * depthFirstTreeWalk(object[name], name);
    }
}

function indent(count) {
    return '    '.repeat(count);
}

function hasMethod(options, name, type) {
    if (name in options.methods[type]) {
        return true;
    }
    if (options.parent != null && hasMethod(options.parent, name, type)) {
        return true;
    }
    for (const mixinName of Object.keys(options.mixins ?? {})) {
        const mixinOptions = options.mixins[mixinName];
        if (hasMixinMethod(mixinOptions, mixinOptions, name, type)) {
            return true;
        }
    }
    return false;
}

function hasMixinMethod(baseOptions, options, name, type) {
    if (name in options.methods[type]) {
        return true;
    }
    if (baseOptions.mixinRoot.name === options.name) {
        return false;
    }
    return options.parent != null && hasMixinMethod(baseOptions, options.parent, name, type);
}

function createTestTreeSource(classList) {
    const code = [`${indent(classList.indent)}options = { ...options };`];
    for (const classOptions of classList) {
        if (classOptions.es5) {
            code.push(...createTestES5ClassSource(classOptions));
        } else {
            code.push(...createTestClassSource(classOptions));
        }
    }
    for (const classOptions of classList) {
        if (classOptions.mixinRoot != null) {
            code.push(`${indent(classList.indent)}Mixin.define(${classOptions.name}, { ...options?.define?.${classOptions.name}, root: ${classOptions.mixinRoot.name} });`);
        }
    }
    for (const classOptions of classList) {
        if (classOptions.mixins != null) {
            for (const mixinName of Object.keys(classOptions.mixins)) {
                code.push(`${indent(classList.indent)}Mixin(${mixinName}).extends(${classOptions.name}, { ...options?.extends?.${mixinName}?.${classOptions.name} })`);
            }
        }
    }
    code.push(`${indent(classList.indent)}return {${classList.map(o => o.name).join(', ')}};`);
    return code;
}

function createTestClassSource(options) {
    let code = `class ${options.name}`;
    if (options.parent != null) {
        code += ` extends ${options.parent.name}`;
    }
    code += ' {';
    code = [`${indent(options.indent)}${code}`];
    code.push(...createTestMethodCode({
        indent: options.indent + 1,
        name: 'constructor',
        static: false,
        type: 'prototype',
        class: options
    }));

    for (const type of ['prototype', 'static']) {
        for (const methodName of Object.keys(options.methods[type])) {
            code.push(...createTestMethodCode({
                ...options.methods[type][methodName],
                indent: options.indent + 1
            }));
        }
    }
    code.push(`${indent(options.indent)}}`);
    return code;
}

function createTestMethodCode(options) {
    const code = [
        `${indent(options.indent)}${options.static ? 'static ' : ''}${options.name}() {`,
        `${indent(options.indent + 1)}const invocation = Object.create(null);`
    ];
    let flagSuperInvoked = false;
    if (!options.static && options.name === 'constructor') {
        if (options.class.super?.constructor === false) {
            code.push(`${indent(options.indent + 1)}invocation.super = null`);
            flagSuperInvoked = true;
        } else if (options.class.parent != null || options.class.super?.constructor === true) {
            code.push(`${indent(options.indent + 1)}invocation.super = super(...arguments).invocation;`);
            flagSuperInvoked = true;
        }
    } else if (options.super ===
        true ||
        options.suoer !==
        false &&
        options.class.parent !=
        null &&
        hasMethod(options.class.parent, options.name, options.type)) {
        code.push(`${indent(options.indent + 1)}invocation.super = super.${options.name}(...arguments);`);
        flagSuperInvoked = true;
    }
    if (!flagSuperInvoked) {
        code.push(`${indent(options.indent + 1)}invocation.super = null`);
    }
    code.push(`${indent(options.indent + 1)}invocation.mixins = Object.create(null);`);
    let mixins = Object.keys(options.class.mixins ?? {});
    if (!options.static && options.name === 'constructor') {
        if (options.class.super != null) {
            for (const mixinName of Object.keys(options.class.super)) {
                if (mixinName === 'constructor') {
                    continue;
                }
                const flag = options.class.super[mixinName];
                const index = mixins.indexOf(mixinName);
                if (flag === false && index >= 0) {
                    mixins.splice(index, 1);
                } else if (flag === true && index < 0) {
                    mixins.push(mixinName);
                }
            }
        }
        for (const mixinName of mixins) {
            const returns = options.class.classMap[mixinName].returnsFromConstructor;
            code.push(`${indent(options.indent + 1)}invocation.mixins.${mixinName} = Mixin(${mixinName}).apply(this, arguments)${returns
                ? ''
                : '.invocation'};`);
        }
    } else {
        mixins = mixins.filter(mixinName => hasMixinMethod(options.class.mixins[mixinName], options.class.mixins[mixinName], options.name, options.type));
        if (options.super != null) {
            for (const mixinName of Object.keys(options.super)) {
                const flag = options.class.super[mixinName];
                const index = mixins.indexOf(mixinName);
                if (flag === false && index >= 0) {
                    mixins.splice(index, 1);
                } else if (flag === true && index < 0) {
                    mixins.push(mixinName);
                }
            }
        }
        for (const mixinName of mixins) {
            code.push(`${indent(options.indent + 1)}invocation.mixins.${mixinName} = ${mixinName}${options.static
                ? ''
                : '.prototype'}.${options.name}.apply(this, arguments);`);
        }
    }
    code.push(`${indent(options.indent + 1)}invocation.this = this;`);
    code.push(`${indent(options.indent + 1)}invocation.arguments = [...arguments];`);
    code.push(`${indent(options.indent + 1)}invocation.newTarget = new.target;`);
    code.push(`${indent(options.indent + 1)}invocation.className = ${JSON.stringify(options.class.name)};`);
    code.push(`${indent(options.indent + 1)}invocation.methodName = ${JSON.stringify(options.name)};`);
    code.push(`${indent(options.indent + 1)}invocation.static = ${JSON.stringify(options.static)};`);
    if (!options.static && options.name === 'constructor') {
        code.push(`${indent(options.indent + 1)}this.invocation = invocation;`);
    } else {
        code.push(`${indent(options.indent + 1)}return invocation;`);
    }
    code.push(`${indent(options.indent)}}`);
    return code;
}

function createTestES5ClassSource(options) {
    const code = [...createTestES5ClassConstructor(options)];
    code.push(`${indent(options.indent)}${options.name}.prototype = Object.create(${options.parent != null ? options.parent.name : 'Object'}.prototype, {`);
    code.push(`${indent(options.indent + 1)}constructor: {`);
    code.push(`${indent(options.indent + 2)}configurable: true,`);
    code.push(`${indent(options.indent + 2)}writable: true,`);
    code.push(`${indent(options.indent + 2)}value: ${options.name}`);
    code.push(`${indent(options.indent + 1)}},`);
    for (const methodName of Object.keys(options.methods.prototype)) {
        code.push(...createTestES5MethodCode({
            ...options.methods.prototype[methodName],
            indent: options.indent + 1
        }));
    }
    code.push(`${indent(options.indent)}});`);
    if (options.parent != null) {
        code.push(`${indent(options.indent)}Object.setPrototypeOf(${options.name}, ${options.parent.name})`);
    }
    code.push(`${indent(options.indent)}Object.defineProperties(${options.name}, {`);
    for (const methodName of Object.keys(options.methods.static)) {
        code.push(...createTestES5MethodCode({
            ...options.methods.static[methodName],
            indent: options.indent + 1
        }));
    }
    code.push(`${indent(options.indent)}});`);
    return code;
}

function createTestES5ClassConstructor(options) {
    const code = [
        `${indent(options.indent)}function ${options.name}() {`,
        `${indent(options.indent + 1)}const invocation = Object.create(null);`
    ];
    if (options.super?.constructor === false) {
        code.push(`${indent(options.indent + 1)}invocation.super = null`);
    } else if (options.parent != null) {
        // ES5 super call cannot be forced. Unlike classes, ES5 has no bound keyword and call the super class by name.
        // So it does not make sense to test "wrong" super, as it will never affect the prototype chain.
        code.push(`${indent(options.indent +
            1)}invocation.super = Reflect.construct(${options.parent.name}, ...arguments, new.target ?? ${options.name}).invocation;`);
    } else {
        code.push(`${indent(options.indent + 1)}invocation.super = null`);
    }
    const mixins = Object.keys(options.mixins ?? {});
    if (options.super != null) {
        for (const mixinName of Object.keys(options.super)) {
            if (mixinName === 'constructor') {
                continue;
            }
            const flag = options.super[mixinName];
            const index = mixins.indexOf(mixinName);
            if (flag === false && index >= 0) {
                mixins.splice(index, 1);
            } else if (flag === true && index < 0) {
                mixins.push(mixinName);
            }
        }
    }
    code.push(`${indent(options.indent + 1)}invocation.mixins = Object.create(null);`);
    for (const mixinName of mixins) {
        const returns = options.classMap[mixinName].returnsFromConstructor;
        code.push(`${indent(options.indent + 1)}invocation.mixins.${mixinName} = Mixin(${mixinName}).apply(this, arguments)${returns ? '' : '.invocation'};`);
    }
    code.push(`${indent(options.indent + 1)}invocation.this = this;`);
    code.push(`${indent(options.indent + 1)}invocation.arguments = [...arguments];`);
    code.push(`${indent(options.indent + 1)}invocation.newTarget = new.target;`);
    code.push(`${indent(options.indent + 1)}invocation.className = ${JSON.stringify(options.name)};`);
    code.push(`${indent(options.indent + 1)}invocation.methodName = ${JSON.stringify('constructor')};`);
    code.push(`${indent(options.indent + 1)}invocation.static = ${JSON.stringify(false)};`);
    code.push(`${indent(options.indent + 1)}this.invocation = invocation;`);
    code.push(`${indent(options.indent)}}`);
    return code;
}

function createTestES5MethodCode(options) {
    const code = [
        `${indent(options.indent)}${options.name}: {`,
        `${indent(options.indent + 1)}configurable: true,`,
        `${indent(options.indent + 1)}writable: true,`,
        `${indent(options.indent + 1)}value: function ${options.name}() {`,
        `${indent(options.indent + 2)}const invocation = Object.create(null);`
    ];
    if (options.class.parent != null && (options.super === true || options.suoer !== false && hasMethod(options.class.parent, options.name, options.type))) {
        code.push(`${indent(options.indent + 2)}invocation.super = ${options.parent.name}${options.static
            ? ''
            : '.prototype'}.${options.name}.apply(this, arguments);`);
    } else {
        code.push(`${indent(options.indent + 2)}invocation.super = null`);
    }
    code.push(`${indent(options.indent + 2)}invocation.mixins = Object.create(null);`);
    let mixins = Object.keys(options.class.mixins ?? {});
    mixins = mixins.filter(mixinName => hasMixinMethod(options.class.mixins[mixinName], options.class.mixins[mixinName], options.name, options.type));
    if (options.super != null) {
        for (const mixinName of Object.keys(options.super)) {
            const flag = options.class.super[mixinName];
            const index = mixins.indexOf(mixinName);
            if (flag === false && index >= 0) {
                mixins.splice(index, 1);
            } else if (flag === true && index < 0) {
                mixins.push(mixinName);
            }
        }
    }
    for (const mixinName of mixins) {
        code.push(`${indent(options.indent + 2)}invocation.mixins.${mixinName} = ${mixinName}${options.static
            ? ''
            : '.prototype'}.${options.name}.apply(this, arguments);`);
    }
    code.push(`${indent(options.indent + 2)}invocation.this = this;`);
    code.push(`${indent(options.indent + 2)}invocation.arguments = [...arguments];`);
    code.push(`${indent(options.indent + 2)}invocation.newTarget = new.target;`);
    code.push(`${indent(options.indent + 2)}invocation.className = ${JSON.stringify(options.class.name)};`);
    code.push(`${indent(options.indent + 2)}invocation.methodName = ${JSON.stringify(options.name)};`);
    code.push(`${indent(options.indent + 2)}invocation.static = ${JSON.stringify(options.static)};`);
    code.push(`${indent(options.indent + 2)}return invocation;`);
    code.push(`${indent(options.indent + 2)}}`);
    code.push(`${indent(options.indent + 1)}},`);
    return code;
}

function augmentMessage(message, text) {
    if (typeof message === 'string' && message.length > 0) {
        return `${message}: ${text}`;
    }
    return text;
}

export function compare(actual, expected, message = '') {
    assert(actual != null, augmentMessage(message, 'Missing invocation objects'));
    for (const property of [
        'className',
        'methodName',
        'static',
        'newTarget',
        'this'
    ]) {
        if (Object.hasOwnProperty.call(expected, property)) {
            assert.strictEqual(actual[property], expected[property], augmentMessage(message, `Invocation '${property}' mismatch`));
        }
    }
    if ('super' in expected) {
        if (expected.super == null) {
            assert(actual.super == null, augmentMessage(message, `super() should not be invoked`));
        } else {
            assert(actual.super != null, augmentMessage(message, `super() must be invoked`));
            compare(actual.super, expected.super, `Mismatch in mixin 'super' invocation`);
        }
    }
    if (Object.hasOwnProperty.call(expected, 'arguments')) {
        assert(actual.arguments != null, augmentMessage(message, 'Missing invocation arguments'));
        assert.strictEqual(actual.arguments.length, expected.arguments.length, augmentMessage(message, `Invocation arguments length mismatch`));
        for (let i = 0; i < expected.arguments.length; ++i) {
            assert.strictEqual(actual.arguments[i], expected.arguments[i], augmentMessage(message, `Invocation argument ${i} mismatch`));
        }
    }
    if (expected.mixins != null) {
        assert(actual.mixins != null, augmentMessage(message, 'Missing invocation mixins'));
        for (const mixinName of Object.keys(expected.mixins)) {
            assert(actual.mixins[mixinName] != null, augmentMessage(message, `Invocation missing mixin '${mixinName}'`));
            compare(actual.mixins[mixinName], expected.mixins[mixinName], augmentMessage(message, `Mismatch in mixin '${mixinName}' invocation`));
        }
        for (const mixinName of Object.keys(actual.mixins)) {
            if (Object.hasOwnProperty.call(expected.mixins, mixinName)) {
                continue;
            }
            assert(false, augmentMessage(message, `Extra mixin '${mixinName}' invoked`));
        }
    }
}
